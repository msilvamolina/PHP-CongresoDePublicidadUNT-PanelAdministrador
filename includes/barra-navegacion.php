<?php 
function comprobar_active($carpeta) {
	$url = $_SERVER['REQUEST_URI']; 
	$explorar_url = explode('/', $url);
	$url_actual = $explorar_url[3];
	$return=NULL;
	if($url_actual==$carpeta) {
		$return = 'active';
	}
	return $return;
}

$total_reportes = $_SESSION['total_reportes'];
$total_negocios_verdes = $_SESSION['total_negocios_verdes'];

?>
<nav class="cd-side-nav">
	<ul>
		<li><img src="<?php echo $Servidor_url; ?>imagenes/logonuevo.png" width="100%" style="padding:10px" alt="Moebius"></li>
		<li class="cd-label">General aplicación</li>
		<?php $active = comprobar_active('notificaciones'); ?>		
		<li class="has-children notifications <?php echo $active; ?>">
			<a href="#0">Notificaciones<span class="count">3</span></a>
			
			<ul>
				<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/notificaciones/01-mandar-notificacion.php">Mandar notificación</a></li>
			</ul>
		</li>

		<li class="cd-label">Cuenta</li>
		<?php $active = comprobar_active('usuario'); ?>		
		<li class="has-children <?php echo $active; ?>">
			<a href="#0">Usuario</a>
		<!-- ejemplo de como seria con un contador al lado
			<a href="#0">Guía de Servicios<span class="count">3</span></a>
		-->	
		<ul>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/01-cambiar-contrasena.php">Cambiar Contraseña</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/usuario/02-modificar-datos.php">Modificar Datos</a></li>
		</ul>
	</li>
	<li class="cd-label">Aplicación</li>
	<?php $active = comprobar_active('informacion-general'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Información General</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/00-cargar-nota.php">Cargar nota</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/04-noticias.php">Notas</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/09-ordenar-notas.php">Ordenar notas</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('conferencistas'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Conferencistas</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/00-cargar-conferencista.php">Cargar conferencista</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/04-conferencistas.php">Conferencistas</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('salas'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Salas</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/salas/00-cargar-sala.php">Cargar sala</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/salas/02-salas.php">Salas</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('eventos'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Eventos</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/00-cargar-evento.php">Cargar evento</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/02-eventos.php">Eventos</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('firebase'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Firebase</a>
		<ul>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/firebase/00-sincronizar-usuarios.php">Sincronizar Usuarios</a></li>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/firebase/01-sincronizar-informacion-general.php">Sincronizar Información Genereal</a></li>
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/firebase/02-sincronizar-conferencistas.php">Sincronizar Conferencistas</a></li>

			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/firebase/03-sincronizar-salas.php">Sincronizar Salas</a></li>

			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/firebase/04-sincronizar-eventos.php">Sincronizar Eventos</a></li>

			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/firebase/05-sincronizar-noticias.php">Sincronizar Noticias</a></li>
		</ul>
	</li>
	<?php $active = comprobar_active('credenciales'); ?>		
	<li class="has-children bookmarks <?php echo $active; ?>">
		<a href="#0">Credenciales</a>
		<ul>				
			<li><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/credenciales/00-imprimir-credenciales.php">Imprimir credenciales</a></li>
		</ul>
	</li>
	<li class="cd-label">Sesión</li>
	<li class="has-children">
		<a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/php/logout.php">Cerrar Sesión</a>
	</ul>
</nav>