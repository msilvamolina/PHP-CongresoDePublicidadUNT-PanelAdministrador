<?php
require_once '../ThumbLib.inc.php';

$ruta = '../../../../APLICACION/Imagenes/notas/';
$ruta_temp = $ruta.'temp/';
$imagen = $ruta_temp.trim($_GET['imagen']);

$tamano[0] = 80;
$tamano[1] = 80;

if($_GET['tamano']=='grande') {
	$tamano[0] = 240;
	$tamano[1] = 240;
	$ruta = $ruta.'grandes/';
}
$thumb = PhpThumbFactory::create($imagen);
$thumb->resize($tamano[0],$tamano[1]);
$thumb->save($ruta.$_GET['imagen']);
$thumb->show();
?>