<?php
require_once '../ThumbLib.inc.php';
$ruta = '../../../../APLICACION/Imagenes/notas/';
$ruta_recorte = $ruta.'recortes/';

$imagen_recorte = explode('.', $_GET['imagen']);

$tipo_recorte = "-miniatura.";
$redimensionar_1 = "280";
$redimensionar_2 = "175";
if($_GET['tipo'] == 'recorte') {
	$tipo_recorte = "-recorte.";
	$redimensionar_1 = "637";
	$redimensionar_2 = "399";	
}

$imagen_nombre = $imagen_recorte[0].$tipo_recorte.$imagen_recorte[1];
$imagen = $ruta.trim($_GET['imagen']);

$tamano = explode('x', trim($_GET['tamano']));
$thumb = PhpThumbFactory::create($imagen);
$thumb->crop($tamano[0],$tamano[1],$tamano[2],$tamano[3]);
$thumb->resize($redimensionar_1, $redimensionar_2);
$thumb->save($ruta_recorte.$imagen_nombre);
$thumb->show();

?>
