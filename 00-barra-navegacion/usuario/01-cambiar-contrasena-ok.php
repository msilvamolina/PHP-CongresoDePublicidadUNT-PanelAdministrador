<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';

$cambiar_contrasena = 1;
$actualizar_datos = 1;
include('../../php/verificar-permisos.php');

$redireccionar = $Servidor_url.'PANELADMINISTRADOR/php/logout.php';
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
			<meta http-equiv="Refresh" content="3; url=<?php echo $redireccionar; ?>" />

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}

	.tabla {
		width: 100%;
	}
	.tabla tr td{
		padding: 10px;
	}	

	.tabla tr:nth-of-type(2n) {
		background: #f5e5f2;
	}

	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">	
			<div class="cd-form floating-labels" style="max-width:1600px">
			<div style="max-width:600px; margin:0 auto;">
			<section id="crear_categoria" >							
				<fieldset >
				  <div class="alert alert-success" role="alert">
			        <strong>Felicitaciones!</strong> la contraseña se cambió correctamente
			      </div>
			      <p>Esperá unos segundos mientras sos redireccionado</p>
		        </fieldset>	
				</section>    	
 				 </div>	
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
<script type="text/javascript">

	function cerrar_popup() {
		$('.cd-popup').removeClass('is-visible');
	}

	function comprobar() {
		var nueva_contrasena = document.getElementById("nueva_contrasena").value; 
		var repetir_contrasena = document.getElementById("repetir_contrasena").value;

		if(nueva_contrasena != repetir_contrasena) { 
			$('#popup_contrasena').addClass('is-visible');
			return false;
		} else {
			return true;
		}
	}
</script>
</body>
</html>