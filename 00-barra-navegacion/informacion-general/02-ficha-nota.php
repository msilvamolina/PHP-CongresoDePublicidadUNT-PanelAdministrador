<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$noticia = trim($_GET['noticia']);
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$noticia) {
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/informacion-general/04-noticias.php';
	header('location:'.$redireccionar);
	exit;
}
conectar2('congreso', "aplicacion");
//consultar en la base de datos
$query_rs_nota = "SELECT noticia_cartucho, noticia_titulo, noticia_bajada, noticia_palabras_claves, fecha_carga, fecha_modificacion, foto_portada, noticia_publicada, fecha_publicacion FROM noticias WHERE id_noticia = $noticia";
$rs_nota = mysql_query($query_rs_nota)or die(mysql_error());
$row_rs_nota = mysql_fetch_assoc($rs_nota);
$totalrow_rs_nota = mysql_num_rows($rs_nota);

$cartucho = $row_rs_nota['noticia_cartucho'];
$titulo = $row_rs_nota['noticia_titulo'];
$bajada = $row_rs_nota['noticia_bajada'];
$palabras_claves = $row_rs_nota['noticia_palabras_claves'];
$fecha_carga = $row_rs_nota['fecha_carga'];
$fecha_modificacion = $row_rs_nota['fecha_modificacion'];
$foto_portada = $row_rs_nota['foto_portada'];

$video_link = $row_rs_nota['video_link'];

$noticia_publicada = $row_rs_nota['noticia_publicada'];
$fecha_publicacion = $row_rs_nota['fecha_publicacion'];

	//consultar en la base de datos
$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_publicacion = $noticia ORDER BY id_foto DESC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/informacion-general/03-recortar-foto.php?origen=ficha&foto='.$id_foto.'&noticia='.$noticia;
	header('location:'.$redireccionar);
	exit;
}

do {
	$id_foto = $row_rs_imagen['id_foto'];
	$array_foto[$id_foto] =  $row_rs_imagen['nombre_foto'];
	$array_fecha_carga[$id_foto] = $row_rs_imagen['fecha_carga'];
	$array_recorte_foto_nombre[$id_foto] =  $row_rs_imagen['recorte_foto_nombre'];
	$array_recorte_foto_miniatura[$id_foto] =  $row_rs_imagen['recorte_foto_miniatura'];
} while($row_rs_imagen = mysql_fetch_assoc($rs_imagen));

//consultar en la base de datos
$query_rs_cuerpo = "SELECT id_cuerpo, orden, cuerpo_tipo, contenido FROM noticias_cuerpo WHERE id_noticia = $noticia ORDER BY orden ASC ";
$rs_cuerpo = mysql_query($query_rs_cuerpo)or die(mysql_error());
$row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo);
$totalrow_rs_cuerpo = mysql_num_rows($rs_cuerpo);


$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/notas/';

$url_imagen = $Servidor_url.'img/usuarios/grandes/';
$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
$icono_boton = 'add.png';
$link_boton = 'agregar_imagen(1)';

if($foto_portada) {
	$imagen = $ruta_imagenes.$array_foto[$foto_portada];
	$nombre_imagen = $usuario_imagen;	
}

do {
	$id_cuerpo = $row_rs_cuerpo['id_cuerpo'];
	$cuerpo_tipo = $row_rs_cuerpo['cuerpo_tipo'];
	$contenido = $row_rs_cuerpo['contenido'];
	$orden = $row_rs_cuerpo['orden'];

	$array_cuerpo[$id_cuerpo] = $contenido;
	$array_cuerpo_tipo[$id_cuerpo] = $cuerpo_tipo;
	$array_cuerpo_orden[$id_cuerpo] = $orden;

	if($cuerpo_tipo=="imagen") {
		$imagen_cuerpo[$contenido] = 1;
	}
} while($row_rs_cuerpo = mysql_fetch_assoc($rs_cuerpo));


desconectar();
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/fichas.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
		.boton_verde a{
			background: #48b617;
			color: #fff;
		}
		.boton_verde a:hover {
			background: #235d09 !important;
			color: #f6ff05;
		}	
		.boton_rojo a{
			background: #c40000;
			color: #fff;
		}
		.boton_rojo a:hover {
			background: #9e0101 !important;
			color: #f6ff05;
		}
		h3 {
			margin-bottom: 5px;
			font-weight: bold;
		}

		.portada {
			color: #2E7D32;
			font-weight: bold;
		}

		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}
		a {
			cursor: pointer;
		}
		.input_video {
			width: 100%;
			padding: 10px;
			margin-top: 15px;
		}

		.video-container {
			position: relative;
			padding-bottom: 56.25%;
			padding-top: 30px; height: 0; overflow: hidden;
		}

		.video-container iframe,
		.video-container object,
		.video-container embed {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
		}
		.video_youtube {
			width: 100%;
		}

		.imagen_cuerpo {
			color: #f90;
			font-weight: bold;
		}

	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_noticia" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta nota?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_noticia"><a onclick="confirmar_borrar_noticia()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->

			<div class="cd-popup" id="popup_categoria" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta imagen?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_categoria"><a onclick="">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->		
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:700px; margin:0 auto;">
					<nav role="navigation">
						<ul class="cd-pagination">
							<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/01-editar-nota.php?noticia=<?php echo $noticia; ?>">Editar</a></li>		
							<li class="button boton_rojo"><a href="#" onclick="borrar_noticia()">Borrar</a></li>

						</ul>
					</nav> <!-- cd-pagination-wrapper -->
					<section id="crear_categoria" >		
						<fieldset style="margin-top:-50px;">
							<div class="row">
								<div class="col-md-6">
									<div id="imagen_cargando" style="display:none">
										<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/loader.gif">
									</div>
									<div class="imagen_contenedor" id="imagen_contenedor">
										<div class="imagen_delete">
											<a href="#" onclick="<?php echo $link_boton; ?>">
												<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/<?php echo $icono_boton;?>" class="icono_boton">
											</a>
										</div>
										<img src="<?php echo $imagen; ?>" class="imagen_usuario">
									</div>					
								</div>
								<div class="col-md-6">
									<div id="txt_usuario_nombre">
										<legend><span><?php echo $titulo; ?></span></legend>
										<?php if($noticia_publicada) { ?>
										<p class="verde">Nota publicada el <?php echo nombre_fecha($fecha_publicacion); ?> <a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/php/05-publicar-nota.php?publicar=0&noticia=<?php echo $noticia; ?>">[Quitar publicación]</a></p>
										<?php } else { ?>
										<p class="rojo">Esta noticia no está publicada <a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/php/05-publicar-nota.php?publicar=1&noticia=<?php echo $noticia; ?>">[Publicar]</a></p>
										<?php } ?>
										<br>
										<p><b>Creada: </b><?php echo nombre_fecha($fecha_carga);?></p>
										<p><b>Última modificación: </b><?php if($fecha_modificacion) { echo nombre_fecha($fecha_modificacion); } else { echo "Nunca se modificó"; } ?></p>
										<p><a href="">Ver nota en el diario</a></p>
									</div>
								</div>
							</div>			

							<h3>Imágenes
								<a href="#" onclick="agregar_imagen(0)"></a></h3>
								<table class="table table-striped">
									<tbody>
										<?php
										if($totalrow_rs_imagen) { 
											foreach ($array_foto as $id_foto => $nombre_foto) {
												$fecha_carga = $array_fecha_carga[$id_foto];
												$recorte_foto_nombre = $array_recorte_foto_nombre[$id_foto];
												$recorte_foto_miniatura = $array_recorte_foto_miniatura[$id_foto];
												?>
												<tr>
													<td width="200"><a target="_blank" href="<?php echo $ruta_imagenes.$nombre_foto; ?>"><img src="<?php echo $ruta_imagenes.$nombre_foto; ?>" width="200px"></a></td>
													<td>
														<p><a class="rojo" onclick="borrar_imagen(<?php echo $id_foto; ?>)">Borrar imagen</a></p>
														<p><b><?php echo $nombre_foto; ?></b> </p>
														<?php if($imagen_cuerpo[$id_foto]) { ?>
														<p class="imagen_cuerpo">Esta imagen está en el cuerpo</p>
														<?php } ?>
														<?php if($foto_portada!=$id_foto) { ?>
														<p><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/php/04-establecer-foto-portada.php?noticia=<?php echo $noticia; ?>&foto=<?php echo $id_foto; ?>">Establecer como imagen de portada</a><p>
															<?php } else { ?>
															<p class="portada">Portada</p>
															<?php } ?>
															<p><?php echo nombre_fecha($fecha_carga); ?><p><br>
																<?php if($recorte_foto_miniatura) { ?>
																<p><i class="fa fa-crop"></i> Recorte miniatura: <a target="_blank" href="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_miniatura; ?>"><?php echo $recorte_foto_miniatura; ?></a><p>
																	<?php } ?>
																	<?php if($recorte_foto_nombre) { ?>
																	<p><i class="fa fa-crop"></i> Recorte grande: <a target="_blank" href="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_nombre; ?>"><?php echo $recorte_foto_nombre; ?></a><p>
																		<?php } ?>
																		<p><a href="<?php echo $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/informacion-general/03-recortar-foto.php?foto='.$id_foto.'&noticia='.$noticia; ?>">Recortar foto</a><p>
																		</td>
																	</tr>
																	<?php }
																} else { ?>
																<tr><td>No hay imágenes</td></tr>
																<?php } ?>
															</tbody>
														</table>	        
														<br><br>
														<table class="table table-striped">
															<tbody>
															
																<tr>
																	<td><b>Título</b></td>
																	<td><?php echo $titulo; ?></td>
																</tr>
															
																<tr>
																	<td><b>Cuerpo</b></td>
																</tr>  
																<?php
																$ruta_img = $ruta_imagenes.'recortes/';
																$i=1;
																foreach ($array_cuerpo as $id_cuerpo => $contenido) {
																	$cuerpo_tipo = $array_cuerpo_tipo[$id_cuerpo];
																	$orden = $array_cuerpo_orden[$id_cuerpo];

																	if($cuerpo_tipo=="imagen") {
																		$imagen = $ruta_img.$array_recorte_foto_nombre[$contenido];
																		?>
																		<tr>
																			<td width="60"><b><?php echo $i; ?></b></td>
																			<td><img src="<?php echo $imagen; ?>" width="100%"></td>
																		</tr>
																		<?php
																	}

																	if($cuerpo_tipo=="texto") { ?>
																	<tr>
																		<td width="60"><b><?php echo $i; ?></b></td>

																		<td><?php echo $contenido; ?></td>

																		<script type="text/javascript">
																			bkLib.onDomLoaded(function() {
																				new nicEditor().panelInstance('area<?php echo $i;?>');
																			});
																		</script>
																	</tr>	  
																	<?php }

																	if($cuerpo_tipo=="video") { ?>
																	<tr>
																		<td width="60"><b><?php echo $i; ?></b></td>

																		<td>
																			<div class="video_youtube">
																				<div class="video-container">
																					<iframe src="https://www.youtube.com/embed/<?php echo $contenido;?>" frameborder="0" ></iframe>
																				</div>
																			</div>
																		</td>
																	</tr>


																	<?php }
																	$i++;
																}
																?>
															</tr>         	
															<?php if($totalrow_rs_direcciones) { ?>              	              
															<tr>
																<td><b>Direcciones</b></td>
																<td><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/07-direcciones-gps.php?noticia=<?php echo $noticia; ?>">Editar Vinculaciones</a></td>
															</tr>
															<div class="tabla_martin">
																<?php $i=1;
																do {
																	$id_direccion = $row_rs_direcciones['id_direccion'];
																	$direccion_nombre = $row_rs_direcciones['direccion_nombre'];
																	$direccion_numero = $row_rs_direcciones['direccion_numero'];
																	$direccion_piso = $row_rs_direcciones['direccion_piso'];
																	$direccion_latitud = $row_rs_direcciones['direccion_latitud'];
																	$direccion_longitud = $row_rs_direcciones['direccion_longitud'];
																	$direccion_ciudad = $row_rs_direcciones['direccion_ciudad'];
																	$direccion_provincia = $row_rs_direcciones['direccion_provincia'];	
																	$mostrar_direccion = trim($direccion_nombre.' '.$direccion_numero.' '.$direccion_piso);
																	?>
																	<tr>
																		<td><b><?php echo $mostrar_direccion; ?></b></td>
																		<td><?php echo $array_ciudades[$direccion_ciudad].', '.$array_provincias[$direccion_provincia]; ?></td>
																	</tr>		
																	<?php if($direccion_latitud) {
																		$latitud_longitud = $direccion_latitud.','.$direccion_longitud;

																		$url_imagen = "http://maps.googleapis.com/maps/api/staticmap?center=".$latitud_longitud."&zoom=17&size=600x300&scale=2&markers=color:red%7Clabel:%7C".$latitud_longitud;?>			
																		<tr>
																			<td><?php echo $direccion_latitud.', '.$direccion_longitud; ?></td>
																			<td><img src="<?php echo $url_imagen; ?>" width="100%" /></td>
																		</tr>				
																		<?php } ?>					
																		<?php $i++; } while($row_rs_direcciones = mysql_fetch_assoc($rs_direcciones)); ?>
																		<?php } ?>	
																	</tbody>
																</table>	
															</div>
														</div> <!-- .content-wrapper -->
													</main> 
													<?php include('../../includes/pie-general.php');?>
													<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
													<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->

													<script type="text/javascript">
														function agregar_imagen(portada) {
															window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/popUps/01-cargar-imagen.php?noticia=<?php echo $noticia; ?>&portada='+portada,'popup','width=400,height=400');
														}

														function agregar_video() {
															$('#popup_video').addClass('is-visible');
														}

														function borrar_imagen(imagen) {
															$('#popup_categoria').addClass('is-visible');
															$('#btn_confirmar_categoria').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/php/11-borrar-imagen-noticia.php?noticia=<?php echo $noticia;?>&foto='+imagen+'">Sí</a>');
														}

														function borrar_noticia() {
															$('#popup_noticia').addClass('is-visible');
															$('#btn_confirmar_noticia').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/php/12-borrar-noticia.php?noticia=<?php echo $noticia;?>">Sí</a>');
														}

														function confirmar_borrar_noticia() {
															window.location.href = "https://www.wavi.com.ar/sistemaParadigma/php/04-borrar-noticia.php?noticia=<?php echo $noticia;?>";
														}
														function cerrar_popup() {
															$('.cd-popup').removeClass('is-visible');
														}

														function confirmar_video_youtube() {
															var video = document.getElementById("input_video").value;
															window.location.href = "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/libres-del-sur/php/06-video-youtube.php?noticia=<?php echo $noticia; ?>&video="+video;
														}
													</script>

												</body>
												</html>