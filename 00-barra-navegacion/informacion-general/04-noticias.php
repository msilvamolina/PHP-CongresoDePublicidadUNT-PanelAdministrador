<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2('congreso', "aplicacion");

//consultar en la base de datos
$query_rs_noticias = "SELECT id_noticia, noticia_titulo, noticia_publicada, fecha_publicacion, foto_portada FROM noticias ORDER BY id_noticia DESC LIMIT $arranca,$limite";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$pagina_actual_variables = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/informacion-general/04-noticias.php?';
if($usuario) {
	$pagina_actual_variables = $pagina_actual_variables.'usuario='.$usuario.'&';
}
if($q) {
	$pagina_actual_variables = $pagina_actual_variables.'q='.$q.'&';
}
$pagina_siguiente = $pagina+2;
$pagina_anterior = $pagina;
$disabled_siguiente = null;
$disabled_anterior = null;
$link_siguiente = $pagina_actual_variables.'pagina='.$pagina_siguiente;
$link_anterior = $pagina_actual_variables.'pagina='.$pagina_anterior;
if($pagina_anterior<=0) {
	$disabled_anterior = 'disabled';
	$link_anterior = null;
}

if(!$totalrow_rs_noticias) {
	$disabled_siguiente = 'disabled';
	$link_siguiente = null;
}
//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, nombre_foto FROM fotos_publicaciones ";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/notas/';

do {
	$id_foto = $row_rs_fotos['id_foto'];
	$nombre_foto = $row_rs_fotos['nombre_foto'];

	$array_fotos[$id_foto] = $ruta.$nombre_foto;
} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
	.rojo {
		color: #F44336;
		font-weight: bold;
	}

	.verde {
		color: #2E7D32;
		font-weight: bold;
	}
	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
		<!-- Contenido de la Pagina-->
			<nav role="navigation">
		<ul class="cd-pagination">
			<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
			<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
		</ul>
	</nav> <!-- cd-pagination-wrapper -->	
			<div class="cd-form floating-labels" style="max-width:100%">
<legend id="txt_nueva_categoria">Notas</legend>

<?php if($totalrow_rs_noticias) { ?>
 <table class="table table-striped">
            <thead class="tabla_encabezado">
              <tr>
                <th><b>#</b></th>
                <th><b>Portada</b></th>
                <th><b>Título</b></th>
                <th><b>Publicada</b></th>
              </tr>
            </thead>
            <tbody>
  				<?php 

				do { 
	  				$id_noticia = $row_rs_noticias['id_noticia'];
					$noticia_titulo = $row_rs_noticias['noticia_titulo'];
					$noticia_publicada = $row_rs_noticias['noticia_publicada'];
					$nombre_foto = $row_rs_noticias['nombre_foto'];
					$fecha_publicacion = $row_rs_noticias['fecha_publicacion'];
					$foto_portada = $row_rs_noticias['foto_portada'];

					$publicada = '<p class="rojo">No está publicada</p>';
					if($noticia_publicada) {
						$publicada = '<p class="verde">'.nombre_fecha($fecha_publicacion).'</p>';
					}

					$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
					$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

					if($foto_portada) {
						$imagen = $array_fotos[$foto_portada];
					}

					$super_class = null;
					if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
						$super_class = 'categorias_con_subgrupos';
					}
					?>
				  <tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/02-ficha-nota.php?noticia=<?php echo $id_noticia; ?>">
		                <td><?php echo $id_noticia; ?></td>
		                <td><img src="<?php echo $imagen; ?>"  width="50"></td>
		                <td><?php echo $noticia_titulo; ?></td>
		                <td width="200"><?php echo $publicada; ?></td>
	              </tr>		
				<?php } while($row_rs_noticias = mysql_fetch_assoc($rs_noticias)); ?>	          	
            </tbody>
          </table>		     
   <?php } else { ?>
	<p>No hay notas</p>
<?php }?>           
          </div>
          	<nav role="navigation">
		<ul class="cd-pagination">
			<li class="button"><a class="<?php echo $disabled_anterior; ?>" href="<?php echo $link_anterior; ?>">Anterior</a></li>
			<li class="button"><a  class="<?php echo $disabled_siguiente; ?>"  href="<?php echo $link_siguiente; ?>">Siguiente</a></li>
		</ul>
	</nav> <!-- cd-pagination-wrapper -->	
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
<script type="text/javascript">
	$('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
	});
</script>
</body>
</html>