<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$pagina= trim($_GET['pagina']);

$limite = 50;
$pagina_get = $pagina;
if(!$pagina_get) {
	$pagina_get=1;
}
if($pagina) {
	$pagina = $pagina-1;
}
$arranca = $pagina*$limite;

conectar2('congreso', "aplicacion");

//consultar en la base de datos
$query_rs_noticias_agrupadas = "SELECT noticias.id_noticia, noticias.noticia_titulo, noticias.noticia_cartucho, noticias.id_grupo, noticias.foto_portada, fotos_publicaciones.nombre_foto FROM noticias, fotos_publicaciones WHERE noticias.foto_portada = fotos_publicaciones.id_foto AND noticias.noticia_publicada = 1 ORDER BY noticias.orden ASC";
$rs_noticias_agrupadas = mysql_query($query_rs_noticias_agrupadas)or die(mysql_error());
$row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas);
$totalrow_rs_noticias_agrupadas = mysql_num_rows($rs_noticias_agrupadas);

$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/notas/';

do {
	$id_noticia = $row_rs_noticias_agrupadas['id_noticia'];
	$noticia_titulo = $row_rs_noticias_agrupadas['noticia_titulo'];
	$id_grupo = $row_rs_noticias_agrupadas['id_grupo'];
	$foto_portada = $row_rs_noticias_agrupadas['foto_portada'];
	$noticia_cartucho = $row_rs_noticias_agrupadas['noticia_cartucho'];
	$recorte_foto_miniatura = $row_rs_noticias_agrupadas['nombre_foto'];

	if(!$array_noticias_grupos[$id_grupo]) {
		$array_noticias_grupos[$id_grupo] = $id_noticia;	
	} else {
		$array_noticias_grupos[$id_grupo] .= '-'.$id_noticia;
	}
	
	$titulo = $noticia_titulo;
	$array_noticias[$id_noticia] = $titulo;
	$array_noticias_imagenes[$id_noticia] = $ruta_imagenes.$recorte_foto_miniatura;

} while($row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas));



$array_grupos[0] = "Sin asignar";

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<link rel='stylesheet' href='<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/styles.css' type='text/css' media='all' />
	<?php include('../../includes/head-general.php'); ?>

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.css" rel="stylesheet">
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-1.7.2.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery-ui.min.js"></script>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/jquery.ui.touch-punch.min.js"></script>

	<script type="text/javascript">

		$(function() {
			$( "#test-list" ).sortable({
				placeholder: "ui-state-highlight",
				opacity: 0.6,
				update: function(event, ui) {
					document.getElementById("boton_submit").disabled = true;
					$('#boton_submit').addClass('boton_trabajando');

					var order = $('#test-list').sortable('serialize');
					var sendInfo = {
						order: order,
					};

					$.ajax({
						type: "POST",
						url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/ajax/01-guardar-orden-noticias-db.php",
						success: function (resultado) {
							$('#input_resultado').val(resultado);
							document.getElementById("boton_submit").disabled = false;
							$('#boton_submit').removeClass('boton_trabajando');
						}, data: sendInfo
					});

				}
			});
			$( "#test-list" ).disableSelection(); 

			$( "#test-list" ).sortable({
				cancel: ".ui-state-disabled"
			});

		});

	</script>
	<style type="text/css">
		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}

		.checkbox {
			z-index: 999;
		}
		h2 {
			font-size: 20px;
		}

		.input_popup {
			width: 100%;
			padding: 10px;
		}

		#agrupadas table td {
			background-color: #FFF9C4;
		}
		#agrupadas table .td_header {
			background-color: #FFC107;
			color: #fff;
		}

		.noticias_principales {
			background:#FC0;
			padding:20px;
			padding-top:1px;
		}


		.noticias_varias {
			background:#ccc;
			padding:20px;
			margin-top:20px;
			padding-top:1px;	
		}

		.appendoButtons{
			font-size:26px;
		}
		#sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
		#sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px; }
		#sortable li span { position: absolute; margin-left: -1.3em; }

		.botones_noticias a{
			background:#F30;
			color:#fff;
			padding:10px;
			font-weight:bold;
			text-decoration:none;
		}

		.botones_noticias a:hover{
			background:#606;
		}

		.lista_header {
			background:#f90!important; 
			color:#fff;
		}
		#contenedor_boton {
			width: 100%;
			text-align: right;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" style="max-width:100%">
			<div class="cd-form floating-labels" style="max-width:100%">
				<div id="resultado">
					<form method="POST" id="formQuitarVinculaciones" action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/php/18-guardar-noticia-grupo-db.php">
						<input type="hidden" required value="" name="orden" id="input_resultado">
						<div id="contenedor_boton">
							<input type="submit" value="Guardar orden" id="boton_submit" class="boton_submit">
						</div>
					</form>
				</div>
				<?php if($_GET['ok']) { ?>
				<div class="alert alert-success" role="alert">
					Los cambios se guardaron correctamente a las <?php echo date('H:i:s'); ?>
				</div>
				<?php } ?>
				<ul id="test-list">
					<?php 
					foreach ($array_grupos as $id_grupo => $grupo_nombre) { ?>

					<?php
					$explorar_noticias = explode("-", $array_noticias_grupos[$id_grupo]);

					foreach ($explorar_noticias as $id_noticia) { 
						if($id_noticia) {
							$noticia_titulo = $array_noticias[$id_noticia];
							$imagen = $array_noticias_imagenes[$id_noticia];?>

							<li id="listItem_<?php echo $id_noticia;?>">
								<table class="table table-striped" >
									<tbody>
										<tr class="<?php echo $super_class; ?>" data-href="noticia<?php echo $id_noticia; ?>">
											<td width="60">
												<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/drag/arrow.png" alt="move" width="16" height="16" class="handle" />
											</td>

											<td width="60">
												<?php echo $id_noticia; ?></td>
												<td width="110">
													<img src="<?php echo $imagen; ?>"  width="50"></td>
													<td><?php echo $noticia_titulo; ?></td>
												</tr>	
											</tbody>
										</table>
										<?php } } } ?>

									</ul>
								</div> <!-- .content-wrapper -->
							</main> 
							<script type="text/javascript">
								document.getElementById("boton_submit").disabled = true;
								$('#boton_submit').addClass('boton_trabajando');
							</script>
						</body>
						</html>