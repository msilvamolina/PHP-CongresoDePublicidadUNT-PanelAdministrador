<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

//eliminamos los temporales

conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_provincia = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincia = mysql_query($query_rs_provincia)or die(mysql_error());
$row_rs_provincia = mysql_fetch_assoc($rs_provincia);
$totalrow_rs_provincia = mysql_num_rows($rs_provincia);

do {
	$id_provincia = $row_rs_provincia['id_provincia'];
	$provincia_nombre = $row_rs_provincia['provincia_nombre'];

	$array_provincia[335] = 'NACIONAL';
	$array_provincia[$id_provincia] = $provincia_nombre;
} while ($row_rs_provincia = mysql_fetch_assoc($rs_provincia));
desconectar();

$array_secciones['NoticiasCiudad'] = '(Predeterminada) Noticias de tu Ciudad';
$array_secciones['NoticiasRecomendadas'] = 'Noticias Recomendadas';

$array_secciones2['NoticiasCiudad'] = 'Noticias de tu Ciudad';
$array_secciones2['NoticiasRecomendadas'] = 'Noticias Recomendadas';

if($verificar_permiso['permisos_wavi_webmaster']) { 
	$array_secciones['Sorteos'] = 'Sorteos';
	$array_secciones2['Sorteos'] = 'Sorteos';
}

conectar2('mywavi', 'sitioweb');

//consultar en la base de datos
$query_rs_diarios = "SELECT id_diario, diario_nombre, diario_imagen, id_provincia, seccion FROM diarios ORDER BY diario_nombre ASC ";
$rs_diarios = mysql_query($query_rs_diarios)or die(mysql_error());
$row_rs_diarios = mysql_fetch_assoc($rs_diarios);
$totalrow_rs_diarios = mysql_num_rows($rs_diarios);

desconectar();


$url_imagen = $Servidor_url.'APLICACION/Imagenes/diarios/';
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip-utils.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/FileSaver.js"></script>

	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
		.no_hay_imagen{
			color: #acacac;
		}
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:600px; margin:0 auto;">
					<section id="crear_categoria" >							
						<fieldset >
							<form action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/php/05-agregar-diario-db.php" method="post">
								<legend id="txt_nueva_categoria">Nuevo diario</legend>	
								<div class="icon">
									<label class="cd-label" for="cd-company">Nombre del diario</label>
									<input class="company" type="text" name="nombre_diario" autofocus required>
								</div> 
								<p class="cd-select">
									<select name="provincia" class="select_class" required>
										<option value="0">Elegí una provincia</option>
										<?php foreach ($array_provincia as $id_provincia => $provincia_nombre) { ?>
										<option value="<?php echo $id_provincia; ?>"><?php echo $provincia_nombre; ?></option>	
										<?php }  ?>
									</select></p><br>
									<p class="cd-select">
										<select name="seccion" class="select_class" required>
											<option value="0">Elegí una sección</option>
											<?php foreach ($array_secciones as $id_seccion => $seccion_nombre) { ?>
											<option value="<?php echo $id_seccion; ?>"><?php echo $seccion_nombre; ?></option>	
											<?php }  ?>
										</select></p>					
										<div class="alinear_centro">
											<button class="boton_azul" id="btn_continuar" >Cargar</button>
										</div>


									</form>
								</fieldset>	
							</section>    	
						</div>		
						<table class="table table-striped">
							<thead class="tabla_encabezado">
								<tr>
									<th><b>#</b></th>
									<th><b>Imagen</b></th>
									<th><b>Nombre</b></th>
									<th><b>Provincia</b></th>
									<th><b>Sección</b></th>
								</tr>
							</thead>
							<tbody>
								<?php do {
									$id_diario = $row_rs_diarios['id_diario'];
									$id_provincia = $row_rs_diarios['id_provincia'];
									$diario_nombre = $row_rs_diarios['diario_nombre'];
									$diario_imagen = $row_rs_diarios['diario_imagen'];
									$seccion = $row_rs_diarios['seccion'];

									$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
									$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
									if($diario_imagen) {
										$imagen = $url_imagen.$diario_imagen;
										$nombre_imagen = $diario_imagen;
									}

									$super_class = null;
									if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
										$super_class = 'categorias_con_subgrupos';
									}
									?>
									<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/06-ficha-diario.php?diario=<?php echo $id_diario; ?>">
										<td><?php echo $id_diario; ?></td>
										<td><img src="<?php echo $imagen; ?>"  height="40"></td>
										<td><?php echo $diario_nombre; ?></td>
										<td><?php echo $array_provincia[$id_provincia]; ?></td>
										<td><?php echo $array_secciones2[$seccion]; ?></td>

									</tr>		
									<?php } while($row_rs_diarios = mysql_fetch_assoc($rs_diarios)); ?>
								</tbody>
							</table>				 

						</div>
					</div> <!-- .content-wrapper -->
				</main> 
				<?php include('../../includes/pie-general.php');?>
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
				<script type="text/javascript">
					$('tr[data-href]').on("click", function() {
						document.location = $(this).data('href');
					});
				</script>
			</body>
			</html>