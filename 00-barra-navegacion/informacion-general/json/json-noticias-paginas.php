<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$pagina = $_GET['pagina'];

if($pagina) {
	$pagina = $pagina - 1;
} else {
	$pagina = 0;
}

$json = file_get_contents('https://www.mywavi.com/PANELADMINISTRADOR/00-barra-navegacion/wavi-noticias/json/json-maestro.json');
$obj = json_decode($json);

$objeto =  $obj->grupo_noticias;


$i=0;
foreach ($objeto as $grupo) {
	$noticias = $grupo->noticias->paginas[$pagina];
	if($noticias) {
		$array_post["grupo_noticias"][$i]["id_grupo"] = $grupo->id_grupo;
		$array_post["grupo_noticias"][$i]["nombre_grupo"] = $grupo->nombre_grupo;

		$array_post["grupo_noticias"][$i]["noticias"] = $noticias;

		$i++;
	}
}

if($array_post) {
	$array_post["fecha_creacion"] = $obj->fecha_creacion;
	$array_post["pagina"] = $pagina+1;
	$array_post["codigo"] = 'OK';
} else {
	$array_post["codigo"] = 'Vacio';
}

header('Content-Type: application/json');
print_r(json_encode($array_post));
?>