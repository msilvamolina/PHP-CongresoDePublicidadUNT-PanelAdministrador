<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$nombre = trim($_GET['nombre']);
$get_categoria = trim($_GET['categoria']);

conectar2('mywavi', 'WAVI');

$busqueda_grupo_categoria = 0;
$busqueda_subgrupo_categoria = 0;
if($get_categoria) {
	$busqueda_subgrupo_categoria = $get_categoria;
}
desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<style type="text/css">
		.contenedor{
			text-align: center;
			margin: 0 auto;
			padding-top: 40px;
		}
		.bender{
			width: 100%;
			max-width: 134px;
		}

		h2 {
			margin-top: 10px;
			font-size: 26px;
		}
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}	
		.cd-form {
			text-align: left;
		}

		.grupo_categoria {
			margin-left: -20px!important;
		}

		#section_categoria {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;
		}

		#section_categoria h3 {
			font-size: 24px;
		}
		.select_class {
			background: #eeeeee !important;
		}

		.section_informacion_bender {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;		
		}

		#section_informacion_bender {
			line-height: 18px;
		}
		.bender_chiquito {
			width: 30px;
		}

		.info_bender {
			margin-bottom: 20px;
		}
		.delete_categoria {
			float: right;
		}
		.delete_categoria img{
			width: 35px;
			margin-top: -5px;
		}
		#negocio_repetido {
			width: 100%;
			padding: 30px;
			background: #464646;
			color:#fff;
		}
		#negocio_repetido b {
			color: #e6d461;
		}

		#negocio_repetido span {
			color: #f92672;
		}

		#negocio_repetido h3 {
			color: #a6db29;
			font-size: 32px;
			margin-bottom: 10px;
		}

		.bg_destino {
			background-color: #FFCA28 !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="contenedor">
				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/07-mover-negocios-db.php" method="POST">
									<legend id="txt_nueva_categoria">Mover negocios</legend>

									<input type="hidden" value="<?php echo $busqueda_grupo_categoria; ?>" name="categoria1" id="select_grupo_categoria_1" />

									<input type="hidden" value="<?php echo $busqueda_grupo_categoria; ?>" name="categoria2" id="select_grupo_categoria_2" />

									<br><br>
									<section id="section_categoria">
										<h3>Origen</h3>
										<div id="demoBasic"  ></div>		

										<p class="cd-select">
											<select name="subgrupo1" class="select_class" id="select_subgrupo_1" >
												<option value="0">Elegí una subcategoría</option>						
											</select></p>
										</section>
										
										<br><br>
										<section id="section_categoria" class="bg_destino">
											<h3>Destino</h3>
											<div id="demoBasic2" ></div>		

											<p class="cd-select">
												<select name="subgrupo2" class="select_class" id="select_subgrupo_2" >
													<option value="0">Elegí una subcategoría</option>						
												</select></p>
											</section>

											<div class="alinear_centro">
												<input type="submit" value="Continuar" id="btn_nueva_categoria">
											</div>
										</div>
									</div>
								</div> <!-- .content-wrapper -->
							</main> 
							<?php include('../../includes/pie-general.php');?>
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

							<script type="text/javascript">

								$.ajax({
									type: "GET",
									url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/bender/json/02-categorias-json.php",
									data: {},
									success: function (data) {;
										$('#demoBasic').ddslick({
											data: data,
											width: 540,
											imagePosition: "left",
											selectText: "Elegí una categoría",
											onSelected: function (data) {
												var valor = data.selectedData.value;
												cargar_subgrupo(valor, 1);
											}
										});	

										$('#demoBasic2').ddslick({
											data: data,
											width: 540,
											imagePosition: "left",
											selectText: "Elegí una categoría",
											onSelected: function (data) {
												var valor = data.selectedData.value;
												cargar_subgrupo(valor, 2);
											}
										});	
									}
								});


								function cargar_subgrupo(categoria, numero) {
									document.getElementById("select_grupo_categoria_"+numero).value = categoria;
									var subgrupo_elegido = <?php echo $busqueda_subgrupo_categoria; ?>;
									var resultado = '<option value="0">Cargando subcategorías...</option>';
									$('#select_subgrupo_'+numero).html(resultado);
									$.ajax({
										url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/bender/json/03-categorias-subgrupos.php?categoria="+categoria+"&subgrupo="+subgrupo_elegido,
										success: function (resultado) {
											$('#select_subgrupo_'+numero).html(resultado);
										}
									});	
								}


								function validar_formulario() {
									var error = null;	
									var total = document.getElementsByName('select_grupo_categoria[]').length;
									for (var i = 1; i <= total; i++) {
										var dato =  document.getElementById('select_grupo_categoria_'+i).value;
										if((!dato)||(dato==0)) {
											error = 'No podés dejar categorías vacías';
										}
									}
									var total2 = document.getElementsByName('subgrupo[]').length;
									for (var i = 1; i <= total; i++) {
										var dato =  document.getElementById('select_subgrupo_'+i).value;
										if((!dato)||(dato==0)) {
											error = 'No podés dejar subcategorías vacías';
										}
									}	

									if(error) {
										alert(error);
										return false;	
									} else {
										return true;
									}
								}

							</script>
						</body>
						</html>