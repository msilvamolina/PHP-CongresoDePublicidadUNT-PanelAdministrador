<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');


?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->

	<style type="text/css">

	.usuario_avatar {
		width: 50px;
		border-radius: 50%;
	}
	td {
		cursor: pointer;
	}
	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
		<!-- Contenido de la Pagina-->

			<div class="cd-form floating-labels" style="max-width:1600px">
			<div style="max-width:600px; margin:0 auto;">
			<section id="crear_categoria" >							
				<fieldset >
					<form action="javascript:buscar_negocio()">

					<legend id="txt_nueva_categoria">Nuevo negocio</legend>
				    <div class="icon">
				    	<label class="cd-label" for="cd-company">Nombre del negocio</label>
						<input class="company" type="text" name="nombre" id="nueva_categoria_nombre" required>
				    </div> 			    
				    <div class="alinear_centro">
					      <button class="boton_azul" id="btn_continuar" >Buscar</button>
				    </div>	
				    </form>

				    </fieldset>	
				</section>    	
 				 </div>	

 				<div id="mostrar_resultado"></div>
						
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->

<script type="text/javascript">

function buscar_negocio() {
	var nombre = document.getElementById("nueva_categoria_nombre").value;	

		$('#btn_continuar').addClass('boton_trabajando');			
		document.getElementById("btn_continuar").disabled = true;
	if(nombre) {
		$.ajax({
			url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/buscar-negocio.php?nombre="+nombre,
			success: function (resultado) {
				$('#mostrar_resultado').html(resultado);
				$('#btn_continuar').removeClass('boton_trabajando');			
				document.getElementById("btn_continuar").disabled = false;
			}
		});
	}
}

	$('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
	});
</script>
</body>
</html>