<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$negocio = trim($_POST['negocio']);
$provincia = trim($_POST['provincia']);
$ciudad = trim($_POST['ciudad']);
$nombre = trim($_POST['nombre']);
$descripcion = trim($_POST['descripcion']);
$palabras_claves = trim($_POST['palabras_claves']);
$email = trim($_POST['email']);
$sitio_web = trim($_POST['sitio_web']);
if((!$nombre)&&(!$descripcion)&&(!$palabras_claves)) {
	$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/03-negocios.php';
	header('location:'.$link);
	exit;
}
if($sitio_web=='http://www.') {
	$sitio_web = null;
}

$array_grupos = $_POST['select_grupo_categoria'];
$array_subgrupos = $_POST['subgrupo'];


$nombre = arreglar_datos_db($nombre);
$descripcion = arreglar_datos_db($descripcion);
$palabras_claves = arreglar_datos_db($palabras_claves);

$fecha_modificacion = date('Y-m-d H:i:s');

conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_negocio = "SELECT usuario_que_carga FROM negocios WHERE id_negocio = $negocio";
$rs_negocio = mysql_query($query_rs_negocio)or die(mysql_error());
$row_rs_negocio = mysql_fetch_assoc($rs_negocio);
$totalrow_rs_negocio = mysql_num_rows($rs_negocio);

if(!$totalrow_rs_negocio) {
	$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/01-negocios.php';
	header('location:'.$link);
	exit;
}

$usuario_que_carga = $row_rs_negocio['usuario_que_carga'];

mysql_query("UPDATE negocios SET negocio_nombre='$nombre', negocio_descripcion='$descripcion', negocio_palabras_claves='$palabras_claves',negocio_emails='$email', negocio_sitio_web='$sitio_web', negocio_provincia='$provincia', negocio_ciudad='$ciudad', usuario_que_modifica='$id_administrador',ip_visitante_modificacion='$ip_visitante',fecha_modificacion='$fecha_modificacion' WHERE id_negocio='$negocio'");
$direcciones_para_agregar = null;
if($_POST['direccion_nombre']) {
	foreach ($_POST['direccion_nombre'] as $clave => $direccion_nombre) {
		if(trim($direccion_nombre)) {
			$direccion_nombre = arreglar_datos_db($direccion_nombre);
			$direccion_numero = $_POST['direccion_numero'][$clave];
			$direccion_piso = $_POST['direccion_piso'][$clave];
			$provincia_direccion = $_POST['select_provincia'][$clave];
			$ciudad_direccion =$_POST['select_ciudad'][$clave];
			if($_POST['direccion_id'][$clave]) {
				$id_direccion = $_POST['direccion_id'][$clave];
				mysql_query("UPDATE direcciones SET direccion_nombre='$direccion_nombre', direccion_numero='$direccion_numero', direccion_piso='$direccion_piso',direccion_ciudad='$ciudad_direccion', direccion_provincia='$provincia_direccion', usuario_que_modifica='$id_administrador', ip_visitante_modificacion='$ip_visitante', fecha_modificacion='$fecha_modificacion' WHERE id_direccion='$id_direccion'");
			} else {
				if($direccion_nombre) {
					if(!$direcciones_para_agregar) {
						$direcciones_para_agregar = "('".$negocio."', '".$direccion_nombre."', '".$direccion_numero."', '".$direccion_piso."', '".$ciudad_direccion."', '".$provincia_direccion."', '".$id_administrador."', '".$ip_visitante."')";
					} else {
						$direcciones_para_agregar = $direcciones_para_agregar.", ('".$negocio."', '".$direccion_nombre."', '".$direccion_numero."', '".$direccion_piso."', '".$ciudad_direccion."', '".$provincia_direccion."', '".$id_administrador."', '".$ip_visitante."')";				
					}
				}
			}
		}
	}
}
//borramos las direcciones que borro el usuario
//cargamos las direcciones
if($direcciones_para_agregar) {
	$query2 = "INSERT INTO direcciones (id_negocio, direccion_nombre, direccion_numero, direccion_piso, direccion_ciudad, direccion_provincia, usuario_que_crea, ip_visitante) 
	VALUES ".$direcciones_para_agregar;
	$result = mysql_query($query2);
}

//cargamos los telefonos
$telefonos_para_agregar = null;
if($_POST['telefono']) {
	foreach ($_POST['telefono'] as $clave => $telefono) {
		if(trim($telefono)) {
			$codigo_area = $_POST['cod_area'][$clave];
			if($_POST['telefono_id'][$clave]) {
				$id_telefono = $_POST['telefono_id'][$clave];
				mysql_query("UPDATE telefonos SET telefono_caracteristica='$codigo_area', telefono_numero='$telefono', usuario_que_modifica='$id_administrador', ip_visitante_modificacion='$ip_visitante', fecha_modificacion='$fecha_modificacion' WHERE id_telefono='$id_telefono'");
			} else {
				if($telefono) {
					if(!$telefonos_para_agregar) {
						$telefonos_para_agregar = "('".$negocio."', '".$codigo_area."','".$telefono."', '".$ciudad."', '".$provincia."', '".$id_administrador."', '".$ip_visitante."')";
					} else {
						$telefonos_para_agregar = $telefonos_para_agregar.", ('".$negocio."', '".$codigo_area."','".$telefono."', '".$ciudad."', '".$provincia."', '".$id_administrador."', '".$ip_visitante."')";
					}
				}
			}
		}
	}
}
if($telefonos_para_agregar) {
	$query3 = "INSERT INTO telefonos (id_negocio, telefono_caracteristica, telefono_numero, telefono_ciudad, telefono_provincia, usuario_que_crea, ip_visitante) 
	VALUES ".$telefonos_para_agregar;
	$result = mysql_query($query3);
}	

//aca trabajamos con las vinculaciones a las categoria, lo que hacemos es borrar todas las vinculaciones
//y dp cargar las que vienen
//ahora analizamos todo esto
foreach ($array_grupos as $id_grupo => $valor_grupo) {
	if($valor_grupo) {
		//aramamos un array con el grupo y el subgrupo
		$subgrupo = $array_subgrupos[$id_grupo];
		$db_array_grupo_subgrupo[] = $valor_grupo.'=>'.$subgrupo;
	}
}
$db_array_grupo_subgrupo = array_unique($db_array_grupo_subgrupo);

mysql_query("DELETE FROM vinculacion_negocio_categorias WHERE id_negocio=$negocio");

if($db_array_grupo_subgrupo) {
	foreach ($db_array_grupo_subgrupo as $valor) {
		if($valor) {
			$explorar_categoria = explode('=>', $valor);
			$categoria = $explorar_categoria[0];
			$subcategoria = $explorar_categoria[1];

			$query = "INSERT INTO vinculacion_negocio_categorias (id_negocio, id_grupo_categoria, id_subgrupo_categoria, usuario_que_crea, ip_visitante) 
			VALUES ('$negocio','$categoria','$subcategoria','$id_administrador', '$ip_visitante')";
			$result = mysql_query($query);
		}
	}
}

desconectar();
$notificacion_mensaje = $administrador_mostrar_como.' editó un negocio';
$notificacion_dato = 'id_negocio:'.$negocio;
include('../../../php/mandar-notificacion-include-nuevo.php');

$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/02-cargar-negocio-paso4.php?negocio='.$negocio.'&source=editar';
header('location:'.$link);
exit;
?>