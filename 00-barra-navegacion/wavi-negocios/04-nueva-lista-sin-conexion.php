<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$provincia = $_GET['provincia'];
$ciudad = $_GET['ciudad'];

conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre, id_provincia  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$ciudad_provincia = $row_rs_array_ciudades['id_provincia'];
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades[$id_ciudad] = $ciudad_nombre;

	if(!$array_provincias_ciudades[$ciudad_provincia]) {
		$array_provincias_ciudades[$ciudad_provincia] = $id_ciudad;
	} else {
		$array_provincias_ciudades[$ciudad_provincia] .= '-'.$id_ciudad;
	}
	
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));


desconectar();


$nombre_lista = "Lista de ".$array_provincias[$provincia];

if($ciudad) {
	$nombre_lista = "Lista de ".$array_ciudades[$ciudad].", ".$array_provincias[$provincia];
} else {
	$ciudad=0;
}


?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->

	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}

	.tabla {
		width: 100%;
	}
	.tabla tr td{
		padding: 10px;
	}	

	.tabla tr:nth-of-type(2n) {
		background: #f5e5f2;
	}
	.no_hay_imagen{
		color: #acacac;
	}
	.tabla_encabezado {
		color: red;
	}

	tr {
		cursor: pointer;
	}

	.categorias_con_subgrupos {
		background: #f90 !important;
		color: #fff !important;
	}

	.grupo_imagen {
		width: 60px;
		border-radius: 50%;
	}

	.usuario_avatar {
		width: 50px;
		border-radius: 50%;
	}
	td {
		cursor: pointer;
	}

	.table {
		margin-left: 60px;
	}
	</style>
</head>
<body>
<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
	<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
		<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:600px">
<legend id="txt_nueva_categoria">

<b>Nueva lista sin conexión</b></legend>
<?php if($_GET['error']) { ?>
	<p style="color:red">Ya existe una lista vinculada a esta provincia y ciudad</p>
<?php } ?>
<br>
<h2><b>Provincia</b></h2>
	<form method="get" action="<?php echo $Servidor_url_documento;?>" name="form2">
	<p class="cd-select">
		<select name="provincia" class="select_class" id="select_subgrupo_1" onchange="document.forms.form2.submit()">
		<option value="0">Ninguna provincia seleccionada</option>	

		<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) { 
				$selected = null;

				if($provincia==$id_provincia) {
					$selected = 'selected';
				}
		?>
		<option <?php echo $selected; ?> value="<?php echo $id_provincia; ?>"><?php echo $provincia_nombre; ?></option>	
		<?php } ?>

	</select></p>
	</form>
	<?php if($provincia) { ?>
	<form method="get" action="<?php echo $Servidor_url_documento;?>" name="form3">
	<input type="hidden" name="provincia" value="<?php echo $provincia; ?>" >

	<p class="cd-select">
		<select name="ciudad" class="select_class" id="select_subgrupo_1" onchange="document.forms.form3.submit()">
		<option value="0">Lista de provincia completa</option>
		<?php 
				$explorar_ciudades = explode('-', $array_provincias_ciudades[$provincia]);

		foreach ($explorar_ciudades as $id_ciudad) { 
				$selected = null;

				if($ciudad==$id_ciudad) {
					$selected = 'selected';
				}
		?>
		<option <?php echo $selected; ?> value="<?php echo $id_ciudad; ?>"><?php echo $array_ciudades[$id_ciudad]; ?></option>	
		<?php } ?>

	</select></p>
	</form>
	<br>

		<fieldset >
			<form action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/04-nueva-lista-sin-conexion-db.php" method="post">
			<input type="hidden" value="<?php echo $provincia; ?>" name="provincia">
			<input type="hidden" value="<?php echo $ciudad; ?>" name="ciudad">
			<legend id="txt_nueva_categoria">Lista</legend>
		    <div class="icon">
		    	<label class="cd-label" for="cd-company">Nombre de la lista</label>
				<input class="company" type="text" name="nombre_lista" id="nombre_lista" value="<?php echo $nombre_lista; ?>" >
		    </div> 		
		    
			<div class="alinear_centro">
		      	<input type="submit" value="Continuar" id="btn_nueva_ciudad">
		    </div>
		    </form>
		    </fieldset>	
	<?php } ?>
			</div>
		</div> <!-- .content-wrapper -->
	</main> 
<?php include('../../includes/pie-general.php');?>
<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
<script type="text/javascript">
	$('tr[data-href]').on("click", function() {
    document.location = $(this).data('href');
	});
</script>
</body>
</html>