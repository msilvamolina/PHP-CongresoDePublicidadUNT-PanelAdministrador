<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$nombre = trim($_GET['nombre']);
$get_categoria = trim($_GET['categoria']);

conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_provincias = "SELECT id_provincia, provincia_nombre FROM provincias ORDER BY provincia_nombre ASC ";
$rs_provincias = mysql_query($query_rs_provincias)or die(mysql_error());
$row_rs_provincias = mysql_fetch_assoc($rs_provincias);
$totalrow_rs_provincias = mysql_num_rows($rs_provincias);
do {
	$id_provincia = $row_rs_provincias['id_provincia'];
	$provincia_nombre = $row_rs_provincias['provincia_nombre'];
	$array_provincias[$id_provincia] = $provincia_nombre;
} while($row_rs_provincias = mysql_fetch_assoc($rs_provincias));

//consultar en la base de datos
$query_rs_array_ciudades = "SELECT id_ciudad, ciudad_nombre  FROM ciudades ORDER BY ciudad_nombre ";
$rs_array_ciudades = mysql_query($query_rs_array_ciudades)or die(mysql_error());
$row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades);
$totalrow_rs_array_ciudades = mysql_num_rows($rs_array_ciudades);

do {
	$id_ciudad = $row_rs_array_ciudades['id_ciudad'];
	$ciudad_nombre = $row_rs_array_ciudades['ciudad_nombre'];
	$array_ciudades_completas[$id_ciudad] = $ciudad_nombre;	
} while($row_rs_array_ciudades = mysql_fetch_assoc($rs_array_ciudades));

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_descripcion, categoria_nombre, categoria_imagen, categoria_tipo FROM grupo_categorias ORDER BY categoria_tipo, categoria_nombre ASC";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

$busqueda_grupo_categoria = 0;
$busqueda_subgrupo_categoria = 0;
if($get_categoria) {
		//consultar en la base de datos
	$query_rs_analizar_categorias = "SELECT id_grupo_categoria, id_subgrupo_categoria FROM categorias WHERE id_categoria = $get_categoria ";
	$rs_analizar_categorias = mysql_query($query_rs_analizar_categorias)or die(mysql_error());
	$row_rs_analizar_categorias = mysql_fetch_assoc($rs_analizar_categorias);
	$totalrow_rs_analizar_categorias = mysql_num_rows($rs_analizar_categorias);
	
	$busqueda_grupo_categoria = $row_rs_analizar_categorias['id_grupo_categoria'];
	$busqueda_subgrupo_categoria = $row_rs_analizar_categorias['id_subgrupo_categoria'];
}
desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.contenedor{
			text-align: center;
			margin: 0 auto;
			padding-top: 40px;
		}
		.bender{
			width: 100%;
			max-width: 134px;
		}

		h2 {
			margin-top: 10px;
			font-size: 26px;
		}
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}	
		.cd-form {
			text-align: left;
		}

		.grupo_categoria {
			margin-left: -20px!important;
		}

		#section_categoria {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;
		}

		#section_categoria h3 {
			font-size: 24px;
		}
		.select_class {
			background: #eeeeee !important;
		}

		.section_informacion_bender {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;		
		}

		#section_informacion_bender {
			line-height: 18px;
		}
		.bender_chiquito {
			width: 30px;
		}

		.info_bender {
			margin-bottom: 20px;
		}
		.delete_categoria {
			float: right;
		}
		.delete_categoria img{
			width: 35px;
			margin-top: -5px;
		}
		#negocio_repetido {
			width: 100%;
			padding: 30px;
			background: #464646;
			color:#fff;
		}
		#negocio_repetido b {
			color: #e6d461;
		}

		#negocio_repetido span {
			color: #f92672;
		}

		#negocio_repetido h3 {
			color: #a6db29;
			font-size: 32px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="contenedor">


				<div >					<!-- Contenido de la Pagina-->	

					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/php/02-cargar-negocio-paso3-db.php" method="POST">
									<input  type="hidden" id="categoria_contador" value='2'>
									<input  type="hidden" id="direcciones_contador" value='2'>
									<input  type="hidden" id="telefonos_contador" value='1'>
									<input type="hidden" name="pagina_que_manda" value="<?php echo $_SERVER['PHP_SELF']; ?>" />

									<legend id="txt_nueva_categoria">Nuevo negocio</legend>

									<input type="hidden" value="<?php echo $busqueda_grupo_categoria; ?>" name="select_grupo_categoria[]" id="select_grupo_categoria_1" />
									<br><br>
									<section id="section_categoria">
										<h3>Elegí una categoría y subcategoría</h3>
										<div id="demoBasic"  ></div>		

										<p class="cd-select">
											<select name="subgrupo[]" class="select_class" id="select_subgrupo_1" >
												<option value="0">Elegí una subcategoría</option>						
											</select></p>
										</section>
										<div id="div_agregar_categorias"></div>
										<div class="alinear_centro">
											<a class="boton_azul boton_amarillo" onclick="agregar_categoria()" id="btn_agregar_categoria">[+] Agregar a otra categoría</a>
										</div>
										<br>
										<section id="section_categoria">
											<h3>Elegí una provincia</h3><br>
											<p class="cd-select">
												<select name="provincia" class="select_class" required id="select_provincia" onchange="cargar_ciudades(this.value,0)">									
													<option value="0">Elegí una provincia</option>
													<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
														$elegido = null;
														if($id_provincia==$busqueda_provincia) {
															$elegido = 'selected';
														}
														if($id_provincia) {
															echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
														}
													}
													?>
												</select></p>
												<br><br>
												<h3>Elegí una ciudad</h3><br>

												<p class="cd-select">
													<select name="ciudad" class="select_class" required id="select_ciudad_0" >									
														<option value="0">Elegí una ciudad</option>
														<?php do{
															$id_ciudad = $row_rs_ciudad['id_ciudad'];
															$ciudad_nombre = $row_rs_ciudad['ciudad_nombre'];
															$elegido = null;
															if($id_ciudad==$busqueda_ciudad) {
																$elegido = 'selected';
															}
															if($id_ciudad) {
																echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
															}
															$array_ciudades[$id_ciudad]=$ciudad_nombre;
														} while($row_rs_ciudad = mysql_fetch_assoc($rs_ciudad));
														?>
													</select></p>		
												</section><br><br>							      					
												<div class="icon">
													<label class="cd-label" for="cd-company">Nombre del negocio</label>
													<input class="company" type="text" name="nombre" value="<?php echo $nombre; ?>" id="nueva_categoria_nombre" required>
												</div> 			    
												<div class="icon">
													<label class="cd-label" for="cd-textarea">Descripción del negocio</label>
													<textarea class="message" name="descripcion" id="nueva_categoria_descripcion" ><?php echo $bender_descripcion; ?></textarea>
												</div>
												<div class="icon">
													<label class="cd-label" for="cd-email">Sitio web</label>
													<input class="email" type="text" value="<?php echo $bender_sitio_web; ?>" name="sitio_web" id="cd-email">
												</div>				    
												<div class="icon">
													<label class="cd-label" for="cd-company">Palabras claves</label>
													<input class="company" type="text" name="palabras_claves" required id="nueva_categoria_palabras_claves" >
												</div> 	
												<div class="icon">
													<label class="cd-label" for="cd-email">E-mails</label>
													<input class="email" type="email" name="email" id="cd-email">
												</div>
												<input value="<?php echo $bender_latitud; ?>" name="latitud" type="hidden" />
												<input value="<?php echo $bender_longitud; ?>" name="longitud" type="hidden" />

												<legend id="txt_nueva_categoria">Direcciones</legend>
												<div id="direcciones">
													<div id="direccion_agregada_1">
														<table width="100%" >
															<tr>
																<td >
																	<p class="cd-select icon">
																		<select name="select_provincia[]" class="budget"  onchange="cargar_ciudades(this.value,1)">									
																			<option value="0">Elegí una provincia</option>
																			<?php foreach ($array_provincias as $id_provincia => $provincia_nombre) {
																				$elegido = null;
																				if($id_provincia==$busqueda_provincia) {
																					$elegido = 'selected';
																				}
																				if($id_provincia) {
																					echo '<option value="'.$id_provincia.'" '.$elegido.'>'.$provincia_nombre.'</option>';
																				}
																			}
																			?>
																		</select></p>						    	
																	</td>
																	<td >
																		<p class="cd-select icon">
																			<select name="select_ciudad[]" class="budget" id="select_ciudad_1" >									
																				<option value="0">Elegí una ciudad</option>
																				<?php foreach ($array_ciudades as $id_ciudad => $ciudad_nombre) {
																					$elegido = null;
																					if($id_ciudad==$busqueda_ciudad) {
																						$elegido = 'selected';
																					}
																					if($id_ciudad) {
																						echo '<option value="'.$id_ciudad.'" '.$elegido.'>'.$ciudad_nombre.'</option>';
																					}
																				}
																				?>
																			</select></p>					    	
																		</td>		
																		<td class="td_delete"><a onclick="eliminar_direccion(1)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td>					    				    		
																	</tr>
																</table>					
																<table width="100%" >
																	<tr>
																		<td width="60%">
																			<input  type="text" name="direccion_nombre[]" value="<?php echo $bender_direccion;?>" required placeholder="Dirección" >							
																		</td>						    				    		
																	</tr>
																	<tr><td>&nbsp;</td></tr>
																</table>
															</div>
														</div>
														<div class="alinear_centro">
															<a class="boton_azul boton_amarillo" onclick="agregar_direccion()" id="btn_agregar_direccion">[+] Agregar otra dirección</a>
														</div>
														<legend id="txt_nueva_categoria">Teléfonos</legend>
														<table width="100%" id="telefonos">
															<tr id="telefono_agregado_0">
																<td>
																	<input  type="text" name="cod_area[]" value="<?php echo $bender_codigo_area;?>" required placeholder="Código de área" >							
																	
																</td>
																<td>
																	<input  type="text" name="telefono[]" value="<?php echo $bender_telefono;?>" required placeholder="Teléfono">							
																</td>		
																<td class="td_delete"><a onclick="eliminar_telefono(0)"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td>	    				    				    		
															</tr>
														</table>
														<div class="alinear_centro">
															<a class="boton_azul boton_amarillo" onclick="agregar_telefonos()">[+] Agregar teléfono</a>
														</div>				    
														<br><br><br>

														<div class="alinear_centro">
															<input type="submit" value="Continuar" id="btn_nueva_categoria">
														</div>
													</form>
												</fieldset>	
											</section>    	
											
										</div>
									</div>
								</div> <!-- .content-wrapper -->
							</main> 
							<?php include('../../includes/pie-general.php');?>
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
							<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->



							<script type="text/javascript">

								function cargar_ciudades(provincia,campo) {	
									if(provincia) {
										$('#select_ciudad_'+campo).html('<option value="0">Cargando ciudades...</option>');
										$.ajax({
											url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/cargar-ciudades.php?provincia="+provincia,
											success: function (resultado) {
												$('#select_ciudad_'+campo).html(resultado);
											}
										});
									}			
								}	

								function eliminar_direccion(direccion) {
									$('#direccion_agregada_'+direccion).html('');
									
								}
								function eliminar_telefono(telefono) {
									$('#telefono_agregado_'+telefono).html('');
								}	

								function eliminar_categoria(categoria) {
									$('#nueva_categoria_'+categoria).html('');
								}
//Dropdown plugin data
var ddData = [
<?php
$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";
$i = 1;
do { 
	$id = $row_rs_grupo_categoria['id_grupo_categoria'];
	$nombre = $row_rs_grupo_categoria['categoria_nombre'];
	$imagen = $row_rs_grupo_categoria['categoria_imagen'];
	$descripcion = $row_rs_grupo_categoria['categoria_descripcion'];
	$categoria_tipo = $row_rs_grupo_categoria['categoria_tipo'];

	$selected = 'false';

	$nombre = "(".$categoria_tipo.") ".$nombre;

	if($id==$busqueda_grupo_categoria) {
		$selected = 'true';		
	}
	
	if($imagen) {
		$imagen = $url_imagen.$imagen;
	} else {
		$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
	}

	$descripcion = substr($descripcion, 0, 100);
	$descripcion .= '...';
	?>
	{
		text: "<?php echo $nombre; ?>",
		value: <?php echo $id; ?>,
		selected: <?php echo $selected; ?>,
		description: "<?php echo $descripcion; ?>",
		imageSrc: "<?php echo $imagen; ?>"
		<?php if($i == $totalrow_rs_grupo_categoria) {
			echo '}';
		} else {
			echo '},';
		}

		$i++; } while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria)); ?>
		];

//Dropdown plugin data


$('#demoBasic').ddslick({
	data: ddData,
	width: 540,
	imagePosition: "left",
	selectText: "Elegí una categoría",
	onSelected: function (data) {
		var valor = data.selectedData.value;
		cargar_subgrupo(valor, 1);
	}
});	


function cargar_subgrupo(categoria, numero) {
	document.getElementById("select_grupo_categoria_"+numero).value = categoria;
	var subgrupo_elegido = <?php echo $busqueda_subgrupo_categoria; ?>;
	var resultado = '<option value="0">Cargando subcategorías...</option>';
	$('#select_subgrupo_'+numero).html(resultado);
	$.ajax({
		url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/bender/json/03-categorias-subgrupos.php?categoria="+categoria+"&subgrupo="+subgrupo_elegido,
		success: function (resultado) {
			$('#select_subgrupo_'+numero).html(resultado);
		}
	});	
}

function agregar_telefonos() {
	var telefonos_contador = document.getElementById("telefonos_contador").value;
	var codigo_area = "<?php echo $bender_codigo_area; ?>";
	if(!codigo_area) {
		codigo_area = '0381';
	}
	var variable = '<tr id="telefono_agregado_'+telefonos_contador+'">';
	variable = variable+'<td><input  type="text" name="cod_area[]" value="'+codigo_area+'" required placeholder="Código de área" ></td>';
	variable = variable+'<td><input  type="text" name="telefono[]" required placeholder="Teléfono"></td>';
	variable = variable+'<td class="td_delete"><a onclick="eliminar_telefono('+telefonos_contador+')"><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/delete.png"></a></td></tr>';
	
	document.getElementById("telefonos_contador").value = parseInt(telefonos_contador)+1;		
	$('#telefonos').append(variable);
}			
function agregar_direccion() {	
	var direccion_contador = document.getElementById("direcciones_contador").value;
	$('#btn_agregar_direccion').addClass('boton_trabajando');			
	document.getElementById("btn_agregar_direccion").disabled = true;
	var provincia = document.getElementById("select_provincia").value ;
	var ciudad = document.getElementById("select_ciudad_0").value ;
	$.ajax({
		url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/agregar-direccion.php?provincia="+provincia+"&ciudad="+ciudad+"&contador="+direccion_contador,
		success: function (resultado) {
			$('#direcciones').append(resultado);
			$('#btn_agregar_direccion').removeClass('boton_trabajando');			
			document.getElementById("btn_agregar_direccion").disabled = false;
			document.getElementById("direcciones_contador").value = parseInt(direccion_contador)+1;		
		}
	});			
}

function agregar_categoria() {
	var categoria_contador = document.getElementById("categoria_contador").value;
	$('#btn_agregar_categoria').addClass('boton_trabajando');			
	document.getElementById("btn_agregar_categoria").disabled = true;

	$.ajax({
		url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-negocios/ajax/agregar-categoria.php?numero="+categoria_contador,
		success: function (resultado) {
			$('#div_agregar_categorias').append(resultado);	
			document.getElementById("categoria_contador").value = parseInt(categoria_contador)+1;	
			$('#btn_agregar_categoria').removeClass('boton_trabajando');			
			document.getElementById("btn_agregar_categoria").disabled = false;				
		}
	});		
}


function validar_formulario() {
	var error = null;	
	var total = document.getElementsByName('select_grupo_categoria[]').length;
	for (var i = 1; i <= total; i++) {
		var dato =  document.getElementById('select_grupo_categoria_'+i).value;
		if((!dato)||(dato==0)) {
			error = 'No podés dejar categorías vacías';
		}
	}
	var total2 = document.getElementsByName('subgrupo[]').length;
	for (var i = 1; i <= total; i++) {
		var dato =  document.getElementById('select_subgrupo_'+i).value;
		if((!dato)||(dato==0)) {
			error = 'No podés dejar subcategorías vacías';
		}
	}	

	if(error) {
		alert(error);
		return false;	
	} else {
		return true;
	}
}

</script>
</body>
</html>