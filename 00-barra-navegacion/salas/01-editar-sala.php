<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$sala = trim($_GET['sala']);

if(!$sala) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/00-cargar-sala.php';
	header('Location: '.$redireccion);
	exit;
}


conectar2('congreso', "aplicacion");
//consultar en la base de datos
$query_rs_sala = "SELECT sala_nombre, sala_descripcion, sala_sitio_web, sala_email, sala_direccion_google_maps, sala_ciudad, sala_provincia, sala_latitud, sala_longitud, sala_direccion_calle, sala_direccion_numero, sala_direccion_piso, sala_direccion_dpto
FROM salas WHERE id_sala = $sala ";
$rs_sala = mysql_query($query_rs_sala)or die(mysql_error());
$row_rs_sala = mysql_fetch_assoc($rs_sala);
$totalrow_rs_sala = mysql_num_rows($rs_sala);

if(!$totalrow_rs_sala) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/00-cargar-sala.php';
	header('Location: '.$redireccion);
	exit;
}

$provincia = 25;
$ciudad = 1;


$sala_nombre = $row_rs_sala['sala_nombre'];

desconectar();
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->

	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyA51ZllTKq7CDMNNYUro72e6TnJ8RtEQa4'></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/cargar-negocio-google-map.css"> 

	<style type="text/css">
		.contenedor{
			text-align: center;
			margin: 0 auto;
			padding-top: 40px;
		}
		.bender{
			width: 100%;
			max-width: 134px;
		}

		h2 {
			margin-top: 10px;
			font-size: 26px;
		}
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}	
		.cd-form {
			text-align: left;
		}

		.grupo_categoria {
			margin-left: -20px!important;
		}

		#section_categoria {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;
		}

		#section_categoria h3 {
			font-size: 24px;
		}
		.select_class {
			background: #eeeeee !important;
		}

		.section_informacion_bender {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;		
		}

		#section_informacion_bender {
			line-height: 18px;
		}
		.bender_chiquito {
			width: 30px;
		}

		.info_bender {
			margin-bottom: 20px;
		}
		.delete_categoria {
			float: right;
		}
		.delete_categoria img{
			width: 35px;
			margin-top: -5px;
		}
		#negocio_repetido {
			width: 100%;
			padding: 30px;
			background: #464646;
			color:#fff;
		}
		#negocio_repetido b {
			color: #e6d461;
		}

		#negocio_repetido span {
			color: #f92672;
		}

		#negocio_repetido h3 {
			color: #a6db29;
			font-size: 32px;
			margin-bottom: 10px;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="contenedor">


				<div >					<!-- Contenido de la Pagina-->	

					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<?php if($_GET['ok']) { ?>
								<div class="alert alert-success" role="alert">
									Los cambios se guardaron correctamente a las <?php echo date('H:i:s'); ?>
								</div>
								<?php } ?>
								<form id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/salas/php/01-editar-sala-db.php" method="POST">
									<input  type="hidden" id="sala" name="sala" value='<?php echo $sala; ?>'>

									<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/salas/03-ficha-sala.php?sala=<?php echo $sala;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
										<span class="fa-stack fa-lg pull-left">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
										</span>
										<b>Ficha de la sala</b>
									</a>
									<legend id="txt_nueva_categoria">Editar Sala</legend>

									<div class="icon">
										<label class="cd-label" for="cd-company">Nombre de la sala</label>
										<input class="company" type="text" name="nombre" value="<?php echo $sala_nombre; ?>" id="nueva_categoria_nombre" required>
									</div> 			    

									<div class="alinear_centro">
										<a class="boton_azul boton_amarillo" onclick="validar_formulario()">Guardar Cambios</a>
									</div>
								</div>

							</div> <!-- .content-wrapper -->


						</main> 
						<?php include('../../includes/pie-general.php');?>
						<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
						<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
						<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->


						<script type="text/javascript">

							function validar_formulario() {
								document.getElementById("myForm").submit();
							}


						</script>
					</body>
					</html>