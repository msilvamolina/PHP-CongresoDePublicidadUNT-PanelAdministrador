<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$conferencista = trim($_GET['conferencista']);
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$conferencista) {
	$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/conferencistas/00-cargar-conferencista.php';
	header('location:'.$redirigir);
	exit;
}

conectar2('congreso', "aplicacion");

//consultar en la base de datos
$query_rs_noticias = "SELECT conferencista_nombre, conferencista_biografia FROM conferencistas WHERE id_conferencista = $conferencista ";
$rs_noticias = mysql_query($query_rs_noticias)or die(mysql_error());
$row_rs_noticias = mysql_fetch_assoc($rs_noticias);
$totalrow_rs_noticias = mysql_num_rows($rs_noticias);

$noticia_titulo = $row_rs_noticias['conferencista_nombre'];
$noticia_cuerpo = $row_rs_noticias['conferencista_biografia'];

desconectar();

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form3.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/nicEdit2/nicEdit.js" type="text/javascript"></script>

</head>
<style type="text/css">
	.btn_eliminar {
		text-align: right;
		width: 100%;
	}

	a {
		cursor: pointer;
	}
	.boton_verde a{
		background: #48b617;
		color: #fff;
	}
	.boton_verde a:hover {
		background: #235d09 !important;
		color: #f6ff05;
	}	
	.boton_rojo a{
		background: #c40000;
		color: #fff;
	}
	.boton_rojo a:hover {
		background: #9e0101 !important;
		color: #f6ff05;
	}
	h3 {
		margin-bottom: 5px;
		font-weight: bold;
	}

	.portada {
		color: #2E7D32;
		font-weight: bold;
	}

	.rojo {
		color: #F44336;
		font-weight: bold;
	}

	.verde {
		color: #2E7D32;
		font-weight: bold;
	}
	a {
		cursor: pointer;
	}
	.input_video {
		width: 100%;
		padding: 10px;
		margin-top: 15px;
	}

	.input_texto {
		width: 100%;
		height: 200px;
		padding: 10px;
		margin-top: 15px;
	}


	.video-container {
		position: relative;
		padding-bottom: 56.25%;
		padding-top: 30px; height: 0; overflow: hidden;
	}
	
	.video-container iframe,
	.video-container object,
	.video-container embed {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
	.video_youtube {
		width: 100%;
	}

	#formTexto {
		margin-top: -150px;
		padding: 20px;
	}	

	.img_delete {
		width: 30px;
	}

	.td_delete {
		padding: 10px;
		text-align: right;
		width: 30px;
	}
	.td_delete img {
		width: 30px;
		display: block;
	}
	.txt_categoria_elegida {
		padding: 10px;
		color: #2c97de;
		display: block;
	}
	.grupo_categoria {
		margin-left: -20px!important;
	}

	#section_categoria {
		background: #a7a7a7;
		padding: 30px;
		color: #fff;
	}

	#section_categoria h3 {
		font-size: 24px;
	}
	.select_class {
		background: #eeeeee !important;
	}
	.delete_categoria {
		float: right;
	}
	.delete_categoria img{
		width: 35px;
		margin-top: -5px;
	}
	.boton_amarillo {
		background: #f90;
	}
	.boton_amarillo:hover {
		background: red;
		color: #fff;
	}
</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			
			<div class="contenedor">

				<div >					<!-- Contenido de la Pagina-->	

					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<form onsubmit="return validar_formulario()" id="myForm" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/php/01-editar-conferencista-db.php" method="POST">
									<input type="hidden" name="accion" id="accion" />
									<input type="hidden" name="accionDato" id="accionDato" />

									<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/02-ficha-conferencista.php?conferencista=<?php echo $conferencista;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
										<span class="fa-stack fa-lg pull-left">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
										</span>
										<b>Ficha</b>
									</a>
									<legend id="txt_nueva_categoria">Editar Conferencista</legend>

									<div class="icon">
										<label class="cd-label" for="cd-company">Conferencista</label>
										<input class="company" type="text" name="nombre" value="<?php echo $noticia_titulo; ?>" id="titulo" required>
									</div> 			    

									<input  type="hidden" id="conferencista" name="conferencista" value="<?php echo $conferencista; ?>">
									<br>												
									<div id="cuerpo" style="background: #fff">
										<h3>Biografía:</h3>
										<table class="table table-striped">
											<tbody>
												<tr>
													

													<td>
														<textarea name="biografia" id="area<?php echo $i;?>"><?php echo $noticia_cuerpo; ?></textarea>
														
													</td>

													<script type="text/javascript">
														bkLib.onDomLoaded(function() {
															new nicEditor({fullPanel : true}).panelInstance('area<?php echo $i;?>');
														});
													</script>
												</tr>	

											</tbody>
										</table>
									</div>
									
								</fieldset>	
							</section>    	

							<a name="botonGuardar"></a>
							<div class="alinear_centro">
								<input type="submit" value="Guardar" id="btn_nueva_categoria">
							</div>
						</form>
					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->

			<script type="text/javascript">
				
			</script>
		</body>
		</html>