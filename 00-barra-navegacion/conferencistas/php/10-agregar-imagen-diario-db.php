<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$id_diario = trim($_POST['id_diario']);
$diario_nombre = trim($_POST['diario_nombre']);

if(!$id_diario) {
	exit;
}

$acentos = array('á', 'é', 'í', 'ó', 'ú');
$no_acentos = array('a', 'e', 'i', 'o', 'u');

$nuevo_nombre = quitar_acentos($diario_nombre);

$nuevo_nombre = strtolower($nuevo_nombre);
$nuevo_nombre = str_replace(' ', '_', $nuevo_nombre);

$array_eliminar = array('"', "'");
$nuevo_nombre = str_replace($array_eliminar, '', $nuevo_nombre);
$nuevo_nombre = str_replace($acentos, $no_acentos, $nuevo_nombre);


$nuevo_nombre = $id_diario.'-'.rand().'-'.$nuevo_nombre;

$storeFolder = '../../../../APLICACION/Imagenes/diarios/temp/';

if (!empty($_FILES)) {
	$tempFile = $_FILES['file']['tmp_name'];         

	$explorar_extension = explode('.', $_FILES['file']['name']);
	$extension = end($explorar_extension);
	$nuevo_nombre = $nuevo_nombre.'.'.$extension;
	$targetFile =  $storeFolder. $nuevo_nombre; 

	move_uploaded_file($tempFile,$targetFile);

	conectar2('mywavi', 'sitioweb');

		mysql_query("UPDATE diarios SET diario_imagen='$nuevo_nombre' WHERE id_diario=$id_diario");

	desconectar();
}
?>