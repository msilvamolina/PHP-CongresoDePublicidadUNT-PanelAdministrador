<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$foto = $_GET['foto'];
$evento = $_GET['evento'];
$origen = $_GET['origen'];

conectar2('congreso', "aplicacion");
$query_rs_noticia = "SELECT id_publicacion, nombre_foto, recorte_foto_x, recorte_foto_y, recorte_foto_w, recorte_foto_h  FROM fotos_publicaciones WHERE id_foto = $foto";
$rs_noticia = mysql_query($query_rs_noticia)or die(mysql_error());
$row_rs_noticia = mysql_fetch_assoc($rs_noticia);
$totalrow_rs_noticia = mysql_num_rows($rs_noticia);
$id_noticia = $row_rs_noticia['id_publicacion'];
$foto_seleccionada = $row_rs_noticia['nombre_foto'];
$recorte_foto_x = $row_rs_noticia['recorte_foto_x'];
$recorte_foto_y = $row_rs_noticia['recorte_foto_y'];
$recorte_foto_w = $row_rs_noticia['recorte_foto_w'];
$recorte_foto_h = $row_rs_noticia['recorte_foto_h'];
$recorte_foto = $recorte_foto_x.'x'.$recorte_foto_y.'x'.$recorte_foto_w.'x'.$recorte_foto_h;
desconectar();

$archivo_continuar = "03-ficha-evento.php";

?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/fichas.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
		.boton_verde a{
			background: #48b617;
			color: #fff;
		}
		.boton_verde a:hover {
			background: #235d09 !important;
			color: #f6ff05;
		}	
		.boton_rojo a{
			background: #c40000;
			color: #fff;
		}
		.boton_rojo a:hover {
			background: #9e0101 !important;
			color: #f6ff05;
		}

		h3 {
			margin-bottom: 5px;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_categoria" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta imagen?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_categoria"><a onclick="confirmar_borrado_imagen()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->		
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:700px; margin:0 auto;">
					<section id="crear_categoria" >
						<h1>El recorte se guardó correctamente</h1>
						<nav role="navigation" style="margin-top:-140px">
							<ul class="cd-pagination">
								<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/03-recortar-foto.php?foto=<?php echo $foto; ?>&evento=<?php echo $evento; ?>&reemplazar=1">Recortar de nuevo</a></li>		
								<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/<?php echo $archivo_continuar; ?>?evento=<?php echo $evento; ?>" >Continuar</a></li>
								
							</ul>
						</nav> <!-- cd-pagination-wrapper -->
						<h3>Recorte Minuatura</h3>
						<p><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/libreria-imagenes/scripts/recortar-imagen-coordenadas-guardar-evento.php?tamano=<?php echo $recorte_foto; ?>&imagen=<?php echo $foto_seleccionada; ?>&tipo=miniatura"  width="280" height="175"/></p>
						<br><br><br>
						<h3>Recorte Grande</h3>
						<p><img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/libreria-imagenes/scripts/recortar-imagen-coordenadas-guardar-evento.php?tamano=<?php echo $recorte_foto; ?>&imagen=<?php echo $foto_seleccionada; ?>&tipo=recorte" width="637" height="399"/></p>
						
					</section>
				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script type="text/javascript">
		</script>
	</body>
	</html>