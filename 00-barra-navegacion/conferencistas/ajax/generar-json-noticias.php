<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

conectar2("wavi", "sitioweb");

$fecha_actual = date("Y-m-d H:i:s");

//consultar en la base de datos
$query_rs_grupo_noticias = "SELECT id_grupo, grupo_nombre, cantidad_de_notas FROM grupo_noticias WHERE incluir_en_json = 1 ORDER BY orden ASC ";
$rs_grupo_noticias = mysql_query($query_rs_grupo_noticias)or die(mysql_error());
$row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias);
$totalrow_rs_grupo_noticias = mysql_num_rows($rs_grupo_noticias);

do {
	$id_grupo = $row_rs_grupo_noticias['id_grupo'];
	$grupo_nombre = $row_rs_grupo_noticias['grupo_nombre'];
	$incluir_en_json = $row_rs_grupo_noticias['incluir_en_json'];
	$cantidad_de_notas = $row_rs_grupo_noticias['cantidad_de_notas'];

	$array_grupos[$id_grupo] = $grupo_nombre;
	$array_grupos_incluir_en_json[$id_grupo] = $incluir_en_json;
	$array_grupos_cantidad_de_notas[$id_grupo] = $cantidad_de_notas;

} while ($row_rs_grupo_noticias = mysql_fetch_assoc($rs_grupo_noticias));

$array_grupos = array_filter($array_grupos);


//consultar en la base de datos
$query_rs_noticias_agrupadas = "SELECT noticias.id_noticia, noticias.noticia_titulo,noticias.noticia_bajada, noticias.id_grupo, noticias.foto_portada, noticias.id_diario, diarios.diario_nombre, diarios.diario_imagen, diarios.diario_color, diarios.diario_color_dark FROM noticias, diarios WHERE id_grupo > 0 AND noticias.id_diario = diarios.id_diario ORDER BY orden ASC";
$rs_noticias_agrupadas = mysql_query($query_rs_noticias_agrupadas)or die(mysql_error());
$row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas);
$totalrow_rs_noticias_agrupadas = mysql_num_rows($rs_noticias_agrupadas);

do {
	$id_noticia = $row_rs_noticias_agrupadas['id_noticia'];
	$noticia_titulo = $row_rs_noticias_agrupadas['noticia_titulo'];
	$noticia_bajada = $row_rs_noticias_agrupadas['noticia_bajada'];

	$diario_color = $row_rs_noticias_agrupadas['diario_color'];
	$diario_color_dark = $row_rs_noticias_agrupadas['diario_color_dark'];

	$id_grupo = $row_rs_noticias_agrupadas['id_grupo'];

	$foto_portada = $row_rs_noticias_agrupadas['foto_portada'];
	$diario_imagen = $row_rs_noticias_agrupadas['diario_imagen'];

	$id_diario = $row_rs_noticias_agrupadas['id_diario'];
	$diario_nombre = $row_rs_noticias_agrupadas['diario_nombre'];

	if(!$array_noticias_grupos[$id_grupo]) {
		$array_noticias_grupos[$id_grupo] = $id_noticia;	
	} else {
		$array_noticias_grupos[$id_grupo] .= '-'.$id_noticia;
	}
	
	$array_noticias_agrupadas[$id_noticia] = $noticia_titulo;
	$array_noticias_agrupadas_bajada[$id_noticia] = $noticia_bajada;
	$array_noticias_foto_portada[$id_noticia] = $foto_portada;
	$array_noticias_diario_id_diario[$id_noticia] = $id_diario;
	$array_noticias_diario_nombre[$id_noticia] = $diario_nombre;
	$array_noticias_diario_imagen[$id_noticia] = $diario_imagen;
	$array_noticias_diario_color[$id_noticia] = $diario_color;
	$array_noticias_diario_color_dark[$id_noticia] = $diario_color_dark;
} while($row_rs_noticias_agrupadas = mysql_fetch_assoc($rs_noticias_agrupadas));

//consultar en la base de datos
$query_rs_fotos = "SELECT id_foto, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones ";
$rs_fotos = mysql_query($query_rs_fotos)or die(mysql_error());
$row_rs_fotos = mysql_fetch_assoc($rs_fotos);
$totalrow_rs_fotos = mysql_num_rows($rs_fotos);

$ruta = $Servidor_url.'APLICACION/Imagenes/notas/recortes/';

do {
	$id_foto = $row_rs_fotos['id_foto'];
	$nombre_foto = $row_rs_fotos['recorte_foto_nombre'];
	$nombre_foto2 = $row_rs_fotos['recorte_foto_miniatura'];

	$array_fotos[$id_foto] = $ruta.$nombre_foto;
	$array_fotos2[$id_foto] = $ruta.$nombre_foto2;

} while($row_rs_fotos = mysql_fetch_assoc($rs_fotos));

$i=0;


//comenzamos a crear el json

$array_post["fecha_creacion"] = $fecha_actual; 
foreach ($array_grupos as $id_grupo => $grupo_nombre) {
	$cantidad_notas = $array_grupos_cantidad_de_notas[$id_grupo];

	$array_post["grupo_noticias"][$i]["id_grupo"] = $id_grupo;
	$array_post["grupo_noticias"][$i]["nombre_grupo"] = $grupo_nombre;

	$array = explode('-', $array_noticias_grupos[$id_grupo]);
	$p=0;
	$pagina = 0;
	foreach ($array as $id_noticia) {

		if($p==$cantidad_notas) {
			$pagina++;
			$p=0;
		}

		$url_diario = $Servidor_url.'APLICACION/Imagenes/diarios/';

		$noticia_titulo = $array_noticias_agrupadas[$id_noticia];
		$noticia_bajada = $array_noticias_agrupadas_bajada[$id_noticia];
		$foto_portada = $array_noticias_foto_portada[$id_noticia];
		$diario_imagen = $array_noticias_diario_imagen[$id_noticia];
		$id_diario = $array_noticias_diario_id_diario[$id_noticia];
		$diario_nombre = $array_noticias_diario_nombre[$id_noticia];

		$diario_color = $array_noticias_diario_color[$id_noticia];
		$diario_color_dark = $array_noticias_diario_color_dark[$id_noticia];

		if($diario_color) {
			$diario_color = '#'.$diario_color;
		}

		if($diario_color_dark) {
			$diario_color_dark = '#'.$diario_color_dark;
		}
		$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
		$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';

		if($foto_portada) {
			$imagen = $array_fotos[$foto_portada];
			$imagen2 = $array_fotos2[$foto_portada];
		}

		$super_class = null;
		if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
			$super_class = 'categorias_con_subgrupos';
		}

		$array_paginas[$id_grupo] = $pagina;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["id_noticia"] = $id_noticia;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["titulo"] = $noticia_titulo;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["copete"] = $noticia_bajada;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["imagen"]['recorte'] = $imagen;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["imagen"]['miniatura'] = $imagen2;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["diario"]["id_diario"] = $id_diario;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["diario"]["nombre"] = $diario_nombre;

		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["diario"]["imagen"] = $url_diario.$diario_imagen;
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["diario"]["color"] = $diario_color;		
		$array_post["grupo_noticias"][$i]["noticias"]['paginas'][$pagina][$p]["diario"]["color_dark"] = $diario_color_dark;

		$p++;
	}
	$i++;
}

desconectar();

$nombre_archivo = 'json-maestro.json';
$file = '../json/'.$nombre_archivo;

asort($array_paginas);

$cantidad_paginas = end($array_paginas);

$array_post["cantidad_paginas"] = $cantidad_paginas;

$json_string = json_encode($array_post);
file_put_contents($file, $json_string);

echo $cantidad_paginas;
?>