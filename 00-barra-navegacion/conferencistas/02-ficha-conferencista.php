<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$conferencista = trim($_GET['conferencista']);
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$conferencista) {
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/conferencistas/04-conferencistas.php';
	header('location:'.$redireccionar);
	exit;
}
conectar2('congreso', "aplicacion");
//consultar en la base de datos

//consultar en la base de datos
$query_rs_nota = "SELECT * FROM conferencistas WHERE id_conferencista = $conferencista ";
$rs_nota = mysql_query($query_rs_nota)or die(mysql_error());
$row_rs_nota = mysql_fetch_assoc($rs_nota);
$totalrow_rs_nota = mysql_num_rows($rs_nota);

$titulo = $row_rs_nota['conferencista_nombre'];
$contenido = $row_rs_nota['conferencista_biografia'];
$fecha_carga = $row_rs_nota['fecha_carga'];
$fecha_modificacion = $row_rs_nota['fecha_modificacion'];
$foto_portada = $row_rs_nota['conferencista_imagen'];

$video_link = $row_rs_nota['video_link'];

$noticia_publicada = $row_rs_nota['conferencista_publicado'];
$fecha_publicacion = $row_rs_nota['fecha_publicacion'];

	//consultar en la base de datos
$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_conferencista = $conferencista ORDER BY id_foto DESC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/conferencistas/03-recortar-foto.php?origen=ficha&foto='.$id_foto.'&conferencista='.$conferencista;
	header('location:'.$redireccionar);
	exit;
}

do {
	$id_foto = $row_rs_imagen['id_foto'];
	$array_foto[$id_foto] =  $row_rs_imagen['nombre_foto'];
	$array_fecha_carga[$id_foto] = $row_rs_imagen['fecha_carga'];
	$array_recorte_foto_nombre[$id_foto] =  $row_rs_imagen['recorte_foto_nombre'];
	$array_recorte_foto_miniatura[$id_foto] =  $row_rs_imagen['recorte_foto_miniatura'];
} while($row_rs_imagen = mysql_fetch_assoc($rs_imagen));


$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/conferencistas/';

$url_imagen = $Servidor_url.'img/usuarios/grandes/';
$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
$icono_boton = 'add.png';
$link_boton = 'agregar_imagen(1)';

if($foto_portada) {
	$imagen = $ruta_imagenes.$array_foto[$foto_portada];

	if($array_recorte_foto_nombre[$foto_portada]) {
		$imagen = $ruta_imagenes.'recortes/'.$array_recorte_foto_nombre[$foto_portada];	
	}
}

desconectar();
?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/fichas.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->

	<style type="text/css">
		.boton_verde a{
			background: #48b617;
			color: #fff;
		}
		.boton_verde a:hover {
			background: #235d09 !important;
			color: #f6ff05;
		}	
		.boton_rojo a{
			background: #c40000;
			color: #fff;
		}
		.boton_rojo a:hover {
			background: #9e0101 !important;
			color: #f6ff05;
		}
		h3 {
			margin-bottom: 5px;
			font-weight: bold;
		}

		.portada {
			color: #2E7D32;
			font-weight: bold;
		}

		.rojo {
			color: #F44336;
			font-weight: bold;
		}

		.verde {
			color: #2E7D32;
			font-weight: bold;
		}
		a {
			cursor: pointer;
		}
		.input_video {
			width: 100%;
			padding: 10px;
			margin-top: 15px;
		}

		.video-container {
			position: relative;
			padding-bottom: 56.25%;
			padding-top: 30px; height: 0; overflow: hidden;
		}

		.video-container iframe,
		.video-container object,
		.video-container embed {
			position: absolute;
			top: 0;
			left: 0;
			width: 100%;
			height: 100%;
		}
		.video_youtube {
			width: 100%;
		}

		.imagen_cuerpo {
			color: #f90;
			font-weight: bold;
		}

	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_noticia" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta nota?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_noticia"><a onclick="confirmar_borrar_noticia()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->

			<div class="cd-popup" id="popup_categoria" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta imagen?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_categoria"><a onclick="">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->		
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:700px; margin:0 auto;">
					<nav role="navigation">
						<ul class="cd-pagination">
							<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/01-editar-conferencista.php?conferencista=<?php echo $conferencista; ?>">Editar</a></li>		
							<li class="button boton_rojo"><a href="#" onclick="borrar_noticia()">Borrar</a></li>

						</ul>
					</nav> <!-- cd-pagination-wrapper -->
					<section id="crear_categoria" >		
						<fieldset style="margin-top:-50px;">
							<div class="row">
								<div class="col-md-6">
									<div id="imagen_cargando" style="display:none">
										<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/loader.gif">
									</div>
									<div class="imagen_contenedor" id="imagen_contenedor">
										<div class="imagen_delete">
											<a href="#" onclick="<?php echo $link_boton; ?>">
												<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/<?php echo $icono_boton;?>" class="icono_boton">
											</a>
										</div>
										<img src="<?php echo $imagen; ?>" class="imagen_usuario">
									</div>					
								</div>
								<div class="col-md-6">
									<div id="txt_usuario_nombre">
										<legend><span><?php echo $titulo; ?></span></legend>
										<?php if($noticia_publicada) { ?>
										<p class="verde">Conferencista publicado el <?php echo nombre_fecha($fecha_publicacion); ?> <a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/php/05-publicar-conferencista.php?publicar=0&conferencista=<?php echo $conferencista; ?>">[Quitar publicación]</a></p>
										<?php } else { ?>
										<p class="rojo">Este conferencista no está publicado <a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/php/05-publicar-conferencista.php?publicar=1&conferencista=<?php echo $conferencista; ?>">[Publicar]</a></p>
										<?php } ?>
										<br>
										<p><b>Creada: </b><?php echo nombre_fecha($fecha_carga);?></p>
										<p><b>Última modificación: </b><?php if($fecha_modificacion) { echo nombre_fecha($fecha_modificacion); } else { echo "Nunca se modificó"; } ?></p>
									</div>
								</div>
							</div>			

							<h3>Imágenes
								<a href="#" onclick="agregar_imagen(0)"></a></h3>
								<table class="table table-striped">
									<tbody>
										<?php
										if($totalrow_rs_imagen) { 
											foreach ($array_foto as $id_foto => $nombre_foto) {
												$fecha_carga = $array_fecha_carga[$id_foto];
												$recorte_foto_nombre = $array_recorte_foto_nombre[$id_foto];
												$recorte_foto_miniatura = $array_recorte_foto_miniatura[$id_foto];

												if($recorte_foto_nombre) {
													$nombre_foto = 'recortes/'.$recorte_foto_nombre;
												}
												?>
												<tr>
													<td width="200"><a target="_blank" href="<?php echo $ruta_imagenes.$nombre_foto; ?>"><img src="<?php echo $ruta_imagenes.$nombre_foto; ?>" width="200px"></a></td>
													<td>
														<p><a class="rojo" onclick="borrar_imagen(<?php echo $id_foto; ?>)">Borrar imagen</a></p>
														<p><b><?php echo $nombre_foto; ?></b> </p>
													
														<?php if($foto_portada!=$id_foto) { ?>
														<p><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/php/04-establecer-foto-portada.php?noticia=<?php echo $noticia; ?>&foto=<?php echo $id_foto; ?>">Establecer como imagen de perfil</a><p>
															<?php } else { ?>
															<p class="portada">Portada</p>
															<?php } ?>
															<p><?php echo nombre_fecha($fecha_carga); ?><p><br>

																<?php if($recorte_foto_nombre) { ?>
																<p><i class="fa fa-crop"></i> Recorte grande: <a target="_blank" href="<?php echo $ruta_imagenes.'recortes/'.$recorte_foto_nombre; ?>"><?php echo $recorte_foto_nombre; ?></a><p>
																	<?php } ?>
																	<p><a href="<?php echo $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/conferencistas/03-recortar-foto.php?foto='.$id_foto.'&noticia='.$noticia; ?>">Recortar foto</a><p>
																	</td>
																</tr>
																<?php }
															} else { ?>
															<tr><td>No hay imágenes</td></tr>
															<?php } ?>
														</tbody>
													</table>	        
													<br><br>
													<table class="table table-striped">
														<tbody>

															<tr>
																<td><b>Título</b></td>
																<td><?php echo $titulo; ?></td>
															</tr>

															<tr>
																<td><b>Biografía</b></td>
															</tr>  
															<tr>
																<td width="60"><b><?php echo $i; ?></b></td>

																<td><?php echo $contenido; ?></td>

															</tr>
														</tr>         	

													</tbody>
												</table>	
											</div>
										</div> <!-- .content-wrapper -->
									</main> 
									<?php include('../../includes/pie-general.php');?>
									<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
									<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->

									<script type="text/javascript">
										function agregar_imagen(portada) {
											window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/popUps/01-cargar-imagen.php?conferencista=<?php echo $conferencista; ?>&portada='+portada,'popup','width=400,height=400');
										}

										function agregar_video() {
											$('#popup_video').addClass('is-visible');
										}

										function borrar_imagen(imagen) {
											$('#popup_categoria').addClass('is-visible');
											$('#btn_confirmar_categoria').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/php/11-borrar-imagen-noticia.php?conferencista=<?php echo $conferencista;?>&foto='+imagen+'">Sí</a>');
										}

										function borrar_noticia() {
											$('#popup_noticia').addClass('is-visible');
											$('#btn_confirmar_noticia').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/conferencistas/php/12-borrar-conferencista.php?conferencista=<?php echo $conferencista;?>">Sí</a>');
										}

									
										function cerrar_popup() {
											$('.cd-popup').removeClass('is-visible');
										}

										function confirmar_video_youtube() {
											var video = document.getElementById("input_video").value;
											window.location.href = "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/libres-del-sur/php/06-video-youtube.php?noticia=<?php echo $noticia; ?>&video="+video;
										}
									</script>

								</body>
								</html>