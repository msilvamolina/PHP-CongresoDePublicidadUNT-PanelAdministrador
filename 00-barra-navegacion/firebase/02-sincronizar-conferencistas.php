<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$sincronizar = trim($_GET['sincronizar']);


?>
<!doctype html>
<head>
	<!doctype html>
	<html lang="es" class="no-js">
	<head>
		<?php include('../../includes/head-general.php'); ?>
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
		<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
		<style type="text/css">
			.btn_eliminar {
				text-align: right;
				width: 100%;
			}

			a {
				cursor: pointer;
			}
		</style>
	</head>
	<body>
		<?php include('../../includes/header.php'); ?>
		<main class="cd-main-content">
			<?php include('../../includes/barra-navegacion.php'); ?>
			<div class="content-wrapper">
				<div class="contenedor">


					<h1>¿Estás seguro de querer sincronizar <b>"Conferencistas"</b> copiando nuestra base de datos a la base de datos en tiempo real de firebase?</h1>

					<center style="margin-top: -50px">
						<?php if($sincronizar==1) { ?>

						<div class="alert alert-success" role="alert">
							Los cambios se guardaron correctamente a las <?php echo date('H:i:s');?></div>

							<?php } ?>
							<a href="<?php echo $_SERVER['PHP_SELF'];?>?sincronizar=1" class="vc_btn_largo vc_btn_verde vc_btn_3d" style="width:300px">
								<span class="fa-stack fa-lg pull-left">
									<i class="fa fa-circle fa-stack-2x"></i>
									<i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
								</span>
								<b>Sí, sincronicemos</b>
							</a>
						</center>


					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
			<?php include('../../includes/firebase.php');?>

			<script type="text/javascript">
				<?php
				if($sincronizar==1) {
					conectar2('congreso', "aplicacion");

//consultar en la base de datos
					$query_rs_conferencistas = "SELECT id_conferencista, conferencista_nombre, conferencista_imagen, conferencista_biografia, fecha_carga, fecha_modificacion FROM conferencistas WHERE conferencista_publicado = 1";
					$rs_conferencistas = mysql_query($query_rs_conferencistas)or die(mysql_error());
					$row_rs_conferencistas = mysql_fetch_assoc($rs_conferencistas);
					$totalrow_rs_conferencistas = mysql_num_rows($rs_conferencistas);

					if($totalrow_rs_conferencistas) {
						$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/conferencistas/';
						do {
							$id_conferencista = $row_rs_conferencistas['id_conferencista'];
							$conferencista_nombre = $row_rs_conferencistas['conferencista_nombre'];
							$conferencista_imagen = $row_rs_conferencistas['conferencista_imagen'];
							$conferencista_biografia = $row_rs_conferencistas['conferencista_biografia'];

							$conferencista_biografia = arreglar_datos_db($conferencista_biografia);

							if($conferencista_imagen) {
								$conferencista_imagen = $array_imagenes[$conferencista_imagen]['grande'];
								$conferencista_imagen = $ruta_imagenes.$conferencista_imagen;
							} else {
								$conferencista_imagen = "";
							}
							?>
							firebase.database().ref('conferencistas/conferencista<?php echo $id_conferencista; ?>').set({
								id_conferencista: "<?php echo $id_conferencista; ?>",
								conferencista_nombre: "<?php echo $conferencista_nombre; ?>",
								conferencista_biografia: "<?php echo $conferencista_biografia;?>",
								conferencista_imagen: "<?php echo $conferencista_imagen; ?>"
							});
							<?php } while($row_rs_conferencistas = mysql_fetch_assoc($rs_conferencistas));
						}
						desconectar();
					} ?>
				</script>
			</script>
		</body>
		</html>