<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$sincronizar = trim($_GET['sincronizar']);

//consultar en la base de datos
conectar2('congreso', "sitioweb");

//consultar en la base de datos
$query_rs_usuarios = "SELECT * FROM usuarios";
$rs_usuarios = mysql_query($query_rs_usuarios)or die(mysql_error());
$row_rs_usuarios = mysql_fetch_assoc($rs_usuarios);
$totalrow_rs_usuarios = mysql_num_rows($rs_usuarios);

?>

<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<style type="text/css">
		.btn_eliminar {
			text-align: right;
			width: 100%;
		}

		a {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">


				<h1>¿Estás seguro de querer sincronizar/arreglar <b>"Usuarios"</b> copiando nuestra base de datos a la base de datos en tiempo real de firebase?</h1>

				<center style="margin-top: -50px">
					<?php if($sincronizar==1) { ?>

					<div class="alert alert-success" role="alert">
						Los cambios se guardaron correctamente a las <?php echo date('H:i:s');?></div>

						<?php } ?>
						<a href="<?php echo $_SERVER['PHP_SELF'];?>?sincronizar=1" class="vc_btn_largo vc_btn_verde vc_btn_3d" style="width:300px">
							<span class="fa-stack fa-lg pull-left">
								<i class="fa fa-circle fa-stack-2x"></i>
								<i class="fa fa-refresh fa-stack-1x fa-inverse"></i>
							</span>
							<b>Sí, sincronicemos</b>
						</a>
					</center>


				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
		<?php include('../../includes/firebase.php');?>

		<script type="text/javascript">
			<?php
			if($sincronizar==1) {
				do {
					$id_usuario = $row_rs_usuarios["id_usuario"];
					$fecha_verificacion = $row_rs_usuarios["fecha_verificacion"];
					$fecha_acreditacion = $row_rs_usuarios["fecha_acreditacion"];
					$superadministrador = $row_rs_usuarios["superadministrador"];
					$administrador = $row_rs_usuarios["administrador"];
					$usuario_codigo_barras = $row_rs_usuarios["usuario_codigo_barras"];
					$usuario_email = $row_rs_usuarios["usuario_email"];
					$usuario_dni = $row_rs_usuarios["usuario_dni"];
					$usuario_pertenencia_institucional = $row_rs_usuarios["usuario_pertenencia_institucional"];
					$usuario_condicion = $row_rs_usuarios["usuario_condicion"];
					$usuario_nombre = $row_rs_usuarios["usuario_nombre"];
					$usuario_apellido = $row_rs_usuarios["usuario_apellido"];
					$usuario_genero = $row_rs_usuarios["usuario_genero"];
					$usuario_nacimiento = $row_rs_usuarios["usuario_nacimiento"]; 
					$fecha_carga = $row_rs_usuarios["fecha_carga"]; 
					?>

					//recorremos la base de datos de firebase para ver si existe el usuario que tenemos en la bases de datos, porque a veces no la cargó firebase.
					//si no llega a tener id_usuario, también lo arreglamos porque verificamos que eso nos cagó recién
					firebase.database().ref('usuarios/' + "usuario<?php echo $id_usuario; ?>").once("value", function(snapshot) {
						var userData = snapshot.val();
						cargar = null;
						if (!userData){
							cargar = 1;
						} else {
						var key = snapshot.key; // "ada"
    					var id_usuario = snapshot.child("id_usuario").val(); // "last"
    					if(!id_usuario) {
    						cargar = 1;
    					} 
    				}

    				if(cargar==1) {
    					firebase.database().ref('usuarios/' + "usuario<?php echo $id_usuario; ?>").set({
    						id_usuario: "<?php echo $id_usuario; ?>",
    						fecha_verificacion: "<?php echo $fecha_verificacion; ?>",
    						fecha_acreditacion: "<?php echo $fecha_acreditacion; ?>",
    						superadministrador: "<?php echo $superadministrador; ?>",
    						administrador: "<?php echo $administrador; ?>",
    						usuario_codigo_barras: "<?php echo $usuario_codigo_barras; ?>",
    						usuario_email: "<?php echo $usuario_email; ?>",
    						usuario_dni: "<?php echo $usuario_dni; ?>",
    						usuario_pertenencia_institucional: "<?php echo $usuario_pertenencia_institucional; ?>",
    						usuario_condicion: "<?php echo $usuario_condicion; ?>",
    						usuario_nombre: "<?php echo $usuario_nombre; ?>",
    						usuario_apellido: "<?php echo $usuario_apellido; ?>",
    						usuario_genero: "<?php echo $usuario_genero; ?>",
    						usuario_nacimiento: "<?php echo $usuario_nacimiento; ?>",
    						fecha_carga: "<?php echo $fecha_carga; ?>"

    					});
    				}
    			});
					<?php } while($row_rs_usuarios = mysql_fetch_assoc($rs_usuarios));
				}
				?>
			</script>
		</body>
		</html>