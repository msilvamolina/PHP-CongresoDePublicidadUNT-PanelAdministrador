<div class="tabla_martin">
		<section id="section_direccion" >
			<div clas="tabla_martin_fila" id="tabla">
				<div class="celda1">
					<input class="input_mapa" name="nombre_direccion" id="direccion" value="<?php echo $nombre_calle ?>">
				</div>
				<div class="celda2">
					<a class="vc_btn_largo vc_btn_amarillo vc_btn_3d" style="max-width:100%" id="boton_mapa" onclick="codeAddress()">
						<span class="fa-stack fa-lg pull-left">
							<i class="fa fa-circle fa-stack-2x"></i>
							<i class="fa fa-map-marker fa-stack-1x fa-inverse"></i>
						</span>
						<p>Actualizar Mapa</p>
					</a>
				</div>	
			</div>
			<input class="input_mapa" type="hidden" name="latitud" id="lat">
			<input class="input_mapa"  type="hidden"  name="longitud" id="long" >

			<div clas="tabla_martin_fila sin_fondo">
				<div class="celdaL1">
					<p><b>Latitud: </b><span id="lat_txt"></span></p>
				</div>	
				<div class="celdaL1">
					<p><b>Longitud: </b><span id="long_txt"></span></p>
				</div>
				<div class="clear"></div>
				<br>
				<center>
					<div class="mapa" id="map_canvas"></div>
				</center>	
				<hr class="style-four">
				<br>
			</div>		
		</section>									
	</div>