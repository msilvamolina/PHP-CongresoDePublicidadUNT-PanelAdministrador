<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');


conectar2('mywavi', 'sitioweb');

//consultar en la base de datos
$query_rs_diarios = "SELECT id_version, version, novedades FROM versiones ORDER BY id_version DESC ";
$rs_diarios = mysql_query($query_rs_diarios)or die(mysql_error());
$row_rs_diarios = mysql_fetch_assoc($rs_diarios);
$totalrow_rs_diarios = mysql_num_rows($rs_diarios);

desconectar();

?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> 
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
		.no_hay_imagen{
			color: #acacac;
		}
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-popup" id="popup_borrar" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta versión?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_borrado"><a onclick="confirmar_borrado_version()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:600px; margin:0 auto;">
					<section id="crear_categoria" >							
						<fieldset >
							<form action="javascript:enviar()" method="post">
								<legend id="txt_nueva_categoria">Nueva Versión</legend>	
								<div class="icon">
									<label class="cd-label" for="cd-company">Versión</label>
									<input class="company" type="text" id="version" name="version" autofocus required>
								</div> 
								<div class="icon">
									<label class="cd-label" for="cd-textarea">Novedades</label>
									<textarea class="message" name="novedades" id="novedades" ></textarea>
								</div>
								<div class="alinear_centro">
									<button class="boton_azul" id="btn_continuar" >Cargar</button>
								</div>


							</form>
						</fieldset>	
					</section>    	
				</div>		
				<table class="table table-striped">
					<thead class="tabla_encabezado">
						<tr>
							<th><b>#</b></th>
							<th><b>Version</b></th>
							<th><b>Novedades</b></th>
							<th><b></b></th>
						</tr>
					</thead>
					<tbody>
						<?php do {
							$id_version = $row_rs_diarios['id_version'];
							$version = $row_rs_diarios['version'];
							$novedades = $row_rs_diarios['novedades'];
							?>
							<tr id="btn<?php echo $id_version; ?>">
								<td><?php echo $id_version; ?></td>
								<td><?php echo $version; ?></td>
								<td><?php echo $novedades; ?></td>
								<td><a onclick="borrar_version(<?php echo $id_version; ?>)" ><img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/delete.png" width="25px"/></a></td>
							</tr>		
							<?php } while($row_rs_diarios = mysql_fetch_assoc($rs_diarios)); ?>
						</tbody>
					</table>				 

				</div>
			</div> <!-- .content-wrapper -->
		</main> 
		<?php include('../../includes/pie-general.php');?>
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
		<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->

		<script type="text/javascript">
			var id_version;
			function enviar() {
				$('#btn_continuar').addClass('boton_trabajando');			
				document.getElementById("btn_continuar").disabled = true;
				var version = $("#version").val();
				var novedades = $("#novedades").val();

				if(version!=null) {
					var sendInfo = {
						version: version,
						novedades: novedades,
						token: "<?php echo $token; ?>"
					};

					$.ajax({
						type: "POST",
						url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/ARCHIVOSLOGICOS/01-version-app-json.php",
						dataType: "json",
						success: function (data) {
							response = JSON.stringify(data);
							var json = jQuery.parseJSON(response);
							if(json.respuesta == "OK") {
								window.location.reload();
							} else {
								alert(json.respuesta);
							}
						},

						data: sendInfo
					});
				}
			}
			function borrar_version(id) {
				$('#popup_borrar').addClass('is-visible');
				id_version = id;
			}
			function cerrar_popup() {
				$('.cd-popup').removeClass('is-visible');
			}

			function confirmar_borrado_version() {
				if(version!=null) {
					cerrar_popup();
					$("#btn"+id_version).hide();

					var sendInfo = {
						id_version: id_version,
						token: "<?php echo $token; ?>"
					};

					$.ajax({
						type: "POST",
						url: "<?php echo $Servidor_url;?>PANELADMINISTRADOR/ARCHIVOSLOGICOS/02-version-app-borrar-json.php",
						dataType: "json",
						success: function (data) {
							response = JSON.stringify(data);
							var json = jQuery.parseJSON(response);
							if(json.respuesta == "OK") {
								window.location.reload();
							} else {
								alert(json.respuesta);
							}
						},

						data: sendInfo
					});
				}
			}
		</script>
	</body>
	</html>