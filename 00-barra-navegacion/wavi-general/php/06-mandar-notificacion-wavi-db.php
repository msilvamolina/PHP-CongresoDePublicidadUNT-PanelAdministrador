<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');
$notificacion = trim($_POST['notificacion']);

$link_redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-general/06-mandar-notificacion-wavi.php';

if((!$notificacion)) {
	header('location:'.$link_redireccion);
	exit;
}

$notificacion_mensaje = $notificacion;
$notificacion_dato = '';
include('../../../php/mandar-notificacion-wavi.php');


header('location:'.$link_redireccion.'?ok=1');
exit;
?>