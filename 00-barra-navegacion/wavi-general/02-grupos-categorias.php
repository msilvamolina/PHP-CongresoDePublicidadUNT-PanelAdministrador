<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$tipo = $_GET['tipo'];

if(!$tipo) {
	$tipo = "negocios";
}
//eliminamos los temporales

conectar2('mywavi', 'WAVI');

//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen FROM grupo_categorias WHERE categoria_tipo = '$tipo' ORDER BY categoria_nombre ASC";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

//consultar en la base de datos
$query_rs_subgrupo = "SELECT id_subgrupo, id_grupo_dependiente  FROM subgrupos_categorias ";
$rs_subgrupo = mysql_query($query_rs_subgrupo)or die(mysql_error());
$row_rs_subgrupo = mysql_fetch_assoc($rs_subgrupo);
$totalrow_rs_subgrupo = mysql_num_rows($rs_subgrupo);

//consultar en la base de datos
$query_rs_vinculaciones = "SELECT id_grupo_categoria FROM vinculacion_negocio_categorias ";
$rs_vinculaciones = mysql_query($query_rs_vinculaciones)or die(mysql_error());
$row_rs_vinculaciones = mysql_fetch_assoc($rs_vinculaciones);
$totalrow_rs_vinculaciones = mysql_num_rows($rs_vinculaciones);

do {
	$id_grupo_dependiente = $row_rs_vinculaciones['id_grupo_categoria'];
	$cantidad_negocios[$id_grupo_dependiente]++;
} while($row_rs_vinculaciones = mysql_fetch_assoc($rs_vinculaciones));
do {
	$id_grupo_dependiente = $row_rs_subgrupo['id_grupo_dependiente'];
	$cantidad_subgrupos[$id_grupo_dependiente]++;
} while($row_rs_subgrupo = mysql_fetch_assoc($rs_subgrupo));


desconectar();

$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";

?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jszip-utils.js"></script>
	<script type="text/javascript" src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/FileSaver.js"></script>

	<title>Sistema Paradigma 2.0</title>
	<style type="text/css">
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}

		.tabla {
			width: 100%;
		}
		.tabla tr td{
			padding: 10px;
		}	

		.tabla tr:nth-of-type(2n) {
			background: #f5e5f2;
		}
		.no_hay_imagen{
			color: #acacac;
		}
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<!-- Contenido de la Pagina-->
			<div class="cd-form floating-labels" style="max-width:1600px">
				<legend id="txt_nueva_categoria"><b><?php echo $totalrow_rs_grupo_categoria; ?></b> grupos de categorías</legend>
				<form method="get" action="<?php echo $Servidor_url_documento;?>" name="form2">

					<p class="cd-select">
						<select name="tipo" class="select_class" id="select_subgrupo_1" onchange="document.forms.form2.submit()">
							<?php 
							$tipos_negocios[] = "negocios";
							$tipos_negocios[] = "servicios";
							$tipos_negocios[] = "profesionales";

							foreach ($tipos_negocios as $n_tipo) {

								$selected = null;

								if($n_tipo == $tipo) {
									$selected = 'selected';
								}
								echo '<option value="'.$n_tipo.'" '.$selected.'>'.$n_tipo.'</option>';
							}
							?>
						</select></p>

					</form>
					<br><br>
					<table class="table table-striped">
						<thead class="tabla_encabezado">
							<tr>
								<th><b>#</b></th>
								<th><b>ID</b></th>
								<th><b>Imagen</b></th>
								<th><b>Nombre</b></th>
								<th><b>Subgrupos vinculados</b></th>
								<th><b>Negocios vinculados</b></th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; do { 
								$id_grupo_categoria = $row_rs_grupo_categoria['id_grupo_categoria'];
								$categoria_nombre = $row_rs_grupo_categoria['categoria_nombre'];
								$categoria_imagen = $row_rs_grupo_categoria['categoria_imagen'];

								$total_categorias_vinculadas = $array_grupo[$id_grupo_categoria];
								$total_categorias_subgrupo = $array_subgrupo[$id_grupo_categoria];

								$grupo_cantidad_subgrupos = $cantidad_subgrupos[$id_grupo_categoria];
								$grupo_cantidad_negocios = $cantidad_negocios[$id_grupo_categoria];

								if(!$grupo_cantidad_subgrupos) {
									$grupo_cantidad_subgrupos = 0;
								}

								if(!$grupo_cantidad_negocios) {
									$grupo_cantidad_negocios = 0;
								}
								if(!$total_categorias_vinculadas) {
									$total_categorias_vinculadas = 0;
								}
								if(!$total_categorias_subgrupo) {
									$total_categorias_subgrupo = 0;
								}

								$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
								$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
								if($categoria_imagen) {
									$imagen = $url_imagen.$categoria_imagen;
									$nombre_imagen = $categoria_imagen;
								}

								$super_class = null;
								if($total_categorias_vinculadas!=$total_categorias_subgrupo) {
									$super_class = 'categorias_con_subgrupos';
								}
								?>
								<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/07-grupos-categorias-negocios.php?grupo_categoria=<?php echo $id_grupo_categoria; ?>">
									<td><strong><?php echo $i; ?></strong></td>
									<td><?php echo $id_grupo_categoria; ?></td>
									<td><img src="<?php echo $imagen; ?>"  height="40"></td>
									<td><?php echo $categoria_nombre; ?></td>
									<td><strong><?php echo $grupo_cantidad_subgrupos; ?></strong> subgrupos vinculados</td>
									<td><strong><?php echo $grupo_cantidad_negocios; ?></strong> negocios vinculados</td>

								</tr>		
								<?php $i++; } while($row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria)); ?>	          	
							</tbody>
						</table>				 

					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">
				$('tr[data-href]').on("click", function() {
					document.location = $(this).data('href');
				});
			</script>
		</body>
		</html>