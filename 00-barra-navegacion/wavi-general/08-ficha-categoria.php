<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$categoria = trim($_GET['categoria']);
$get_categoria = trim($_GET['categoria']);

if(!$categoria) {
	$link = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-general/02-grupo-categorias.php';
	header('location:'.$link);
	exit;
}

//borramos los temporales
include('php/borrar-temporales.php');

conectar2('mywavi', 'WAVI');
//consultar en la base de datos
$query_rs_grupo_categoria = "SELECT id_grupo_categoria, categoria_nombre, categoria_imagen, categoria_color, categoria_color_dark FROM grupo_categorias WHERE id_grupo_categoria = $categoria";
$rs_grupo_categoria = mysql_query($query_rs_grupo_categoria)or die(mysql_error());
$row_rs_grupo_categoria = mysql_fetch_assoc($rs_grupo_categoria);
$totalrow_rs_grupo_categoria = mysql_num_rows($rs_grupo_categoria);

$categoria_nombre = $row_rs_grupo_categoria['categoria_nombre'];
$categoria_imagen = $row_rs_grupo_categoria['categoria_imagen'];
$categorias_vinculadas = $row_rs_grupo_categoria['categorias_vinculadas'];

$categoria_color = $row_rs_grupo_categoria['categoria_color'];
$categoria_color_dark = $row_rs_grupo_categoria['categoria_color_dark'];

//consultar en la base de datos
$query_rs_categorias = "SELECT id_categoria, categoria_nombre, categoria_dependiente FROM categorias WHERE id_grupo_categoria = $categoria ";
$rs_categorias = mysql_query($query_rs_categorias)or die(mysql_error());
$row_rs_categorias = mysql_fetch_assoc($rs_categorias);
$totalrow_rs_categorias = mysql_num_rows($rs_categorias);

$query_rs_subgrupo = "SELECT id_subgrupo, subgrupo_nombre, subgrupo_imagen FROM subgrupos_categorias WHERE id_grupo_dependiente = $categoria";
$rs_subgrupo = mysql_query($query_rs_subgrupo)or die(mysql_error());
$row_rs_subgrupo = mysql_fetch_assoc($rs_subgrupo);
$totalrow_rs_subgrupo = mysql_num_rows($rs_subgrupo);

desconectar();

$url_imagen = $Servidor_url."APLICACION/Imagenes/categorias/grandes/";
$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
$icono_boton = 'add.png';
$link_boton = 'agregar_imagen()';
if($categoria_imagen) {
	$imagen = $url_imagen.$categoria_imagen;
	$nombre_imagen = $usuario_imagen;
	$icono_boton = 'delete.png';
	$link_boton = 'borrar_imagen()';	
}


?>
<!doctype html>
<html lang="es" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/dropzone.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/dropzone2.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/dropzone.js"></script>	
	<style type="text/css">

		.contenido_principal h1 {
			font-size: 36px;
		}
		.contenedor_permisos ul {
			width: 100%;
		}
		.contenedor_permisos ul li{
			float: left;
			padding: 0px;
		}	
		.contenedor_permisos .columna_izquierda {
			width: 90%;
		}
		.contenedor_permisos .columna_derecha {
			width: 10%;
		}	
		.contenedor_permisos h3{
			font-size: 20px;
		}
		.clear {
			float: none;
			clear: both;
		}
		.permiso {
			font-size: 18px;
			padding: 20px;
			padding-top: 10px;
		}

		.permiso:nth-of-type(2n) {
			background: #fcf2fa;
		}

		.permiso i {
			font-size: 30px;
			margin-top: 10px;
			margin-right: 10px;
		}
		.notificaciones_general {
			margin-bottom: 30px;
			font-size: 24px;
		}

		.notificaciones_general .titulo {
			color: #f90 !important;		
		}
		.boton_selector #boton_1{
			font-size: 40px;
		}
		.boton_selector a {
			color: #6d2766;
			cursor: pointer;
		}
		.primera_notificacion {
			margin-bottom: 50px;
		}


		.primera_notificacion {
			margin-bottom: 50px;
		}	
		legend span	{
			color: #ce2450;
		}

		.imagen_delete a{
			position: absolute;
		}
		.imagen_contenedor {
			max-width: 240px;
		}

		.imagen_usuario {
			width: 100%;
		}
		#txt_usuario_nombre {
			text-align: left;
			margin-top: 25px;
		}
		@media only screen and (min-width: 992px) {
			#txt_usuario_nombre {
				text-align: right;
				margin-top: 25px;
			}	
		}
		.icono_boton {
			height: 50px;
			width: 50px;
		}
		.icono_drag_drop {
			display: block;
			width: 100%;
			max-width: 276px;
			margin: 0 auto;
			margin-bottom: 15px;
		}	

		#imagen_cargando {
			text-align: center;
		}
		.no_hay_informacion {
			color: #ccc;
		}

		#txt_usuario_nombre {
			line-height: 26px;
		}

		.vincular_categorias ul { 
			list-style: none;
			margin: 5px 20px;
		}
		.vincular_categorias li {
			margin: 0 0 5px 0;
			padding: 2px;
		}

		.li_contenedor {
		}
		.li_contenedor:nth-of-type(1n) {
			padding: 30px;
		}
		.li_contenedor:nth-of-type(2n) {
			background: #e0e0e0;
			padding: 30px;
		}

		.li_contenedor .li_categoria_general {
			font-size: 24px;
			color: #c400a3;
			padding: 10px;

		}

		.li_contenedor .li_categoria_general_chica {
			color: #c400a3;
		}

		.categorias_vinculadas li  {
			padding: 5px;
		}

		.categorias_vinculadas li h3 {
			font-size: 24px;
			color: #c400a3;
			padding: 15px 0px;
		}
		h2 {
			font-size: 24px;
		}

		.ficha_categoria {
			background: #<?php echo $categoria_color; ?>;
			margin-top: -50px;
		}

		.txt_usuario_nombre legend span{
			background: #fff !important;
		}

		.barra_arriba {
			height: 30px;
			background: #<?php echo $categoria_color_dark; ?>;
			text-align: right;
			position: absolute;
		}

		.toolbar {
			height: 30px;
		}
	</style>
	<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.min.js"></script>
	<script>
		$(function() {
  	  // Apparently click is better chan change? Cuz IE?
  	  $('input[type="checkbox"]').change(function(e) {
  	  	var checked = $(this).prop("checked"),
  	  	container = $(this).parent(),
  	  	siblings = container.siblings();

  	  	container.find('input[type="checkbox"]').prop({
  	  		indeterminate: false,
  	  		checked: checked
  	  	});

  	  	function checkSiblings(el) {
  	  		var parent = el.parent().parent(),
  	  		all = true;

  	  		el.siblings().each(function() {
  	  			return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
  	  		});

  	  		if (all && checked) {
  	  			parent.children('input[type="checkbox"]').prop({
  	  				indeterminate: false,
  	  				checked: checked
  	  			});
  	  			checkSiblings(parent);
  	  		} else if (all && !checked) {
  	  			parent.children('input[type="checkbox"]').prop("checked", checked);
  	  			parent.children('input[type="checkbox"]').prop("indeterminate", (parent.find('input[type="checkbox"]:checked').length > 0));
  	  			checkSiblings(parent);
  	  		} else {
  	  			el.parents("li").children('input[type="checkbox"]').prop({
  	  				indeterminate: true,
  	  				checked: false
  	  			});
  	  		}
  	  	}

  	  	checkSiblings(container);
  	  });
  	});
  </script>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<!-- Contenido de la Pagina-->	
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="cd-popup" id="popup_categoria" role="alert">
				<div class="cd-popup-container">
					<p>¿Estás seguro de querer borrar esta imagen?</p>
					<ul class="cd-buttons">
						<li id="btn_confirmar_categoria"><a onclick="confirmar_borrado_imagen()">Sí</a></li>
						<li><a onclick="cerrar_popup()">No</a></li>
					</ul>
					<a href="#0" class="cd-popup-close img-replace"></a>
				</div> <!-- cd-popup-container -->
			</div> <!-- cd-popup -->		
			<div class="cd-form floating-labels" style="max-width:1600px">
				<div style="max-width:600px; margin:0 auto;">
					<section id="crear_categoria" >		
						<fieldset >

							<div class="row ficha_categoria">
								<div class="barra_arriba">
									<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/toolbar.png" class="toolbar">
								</div>
								<div class="col-md-6">
									<div id="imagen_cargando" style="display:none">
										<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/loader.gif">
									</div>
									<div class="imagen_contenedor" id="imagen_contenedor">
										<div class="imagen_delete">
											<a href="#" onclick="<?php echo $link_boton; ?>">
												<img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/img/<?php echo $icono_boton;?>" class="icono_boton">
											</a>
										</div>
										<img src="<?php echo $imagen; ?>" class="imagen_usuario" >
									</div>	
									<div  id="contenedor_drag_drop" style="display:none">
										<div id="dropzone"><form action="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/php/03-agregar-imagen-categoria-db.php" class="dropzone needsclick" id="myAwesomeDropzone">
											<input type="hidden" name="id_categoria" value="<?php echo $categoria;?>">
											<input type="hidden" name="categoria_nombre" value="<?php echo $categoria_nombre;?>">

											<div class="fallback" style="display:none">
												<input name="file" type="file" multiple />
											</div>
											<div class="dz-message needsclick">
												<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/icono-drag-drop.png" class="icono_drag_drop">
												Arrastrá la imagen a este recuadro para subirla.
											</div>
										</form></div>
									</div>						
								</div>
								<div class="col-md-6">
									<div id="txt_usuario_nombre">
										<legend><span style="color:#fff"><?php echo $categoria_nombre; ?></span></legend>
									</div>
								</div>
							</div>			

							<section id="permisos_usuario" >							
								<fieldset >
									<fieldset >
										<form action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/php/04-guardar-colores-categoria.php" method="post">
											<legend id="txt_nueva_categoria">Colores</legend>
											<input value="<?php echo $categoria; ?>" type="hidden" name="categoria">
											<div class="icon">
												<label class="cd-label" for="cd-company">Color</label>
												<input class="company" type="text" name="color" value="<?php echo $categoria_color; ?>" id="nueva_categoria_nombre" autofocus required>
											</div> 
											<div class="icon">
												<label class="cd-label" for="cd-company">Color dark</label>
												<input class="company" type="text" name="colordark" value="<?php echo $categoria_color_dark; ?>" id="nueva_categoria_nombre" autofocus required>
											</div> 
											<div class="alinear_centro">
												<button class="boton_azul" id="btn_continuar" >Guardar Colores</button>
											</div>
										</form>
									</fieldset>	
									<fieldset >
										<form action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/php/07-guardar-subgrupos-categoria.php" method="post">
											<legend id="txt_nueva_categoria">Subcategorías</legend>
											<input value="<?php echo $categoria; ?>" type="hidden" name="categoria">
											<p class="cd-select">
												<select  onchange="analizar_select()" class="select_class" id="subgrupo" >
													<option value="0">Nueva subcategoría</option>	
													<?php 
													if($totalrow_rs_subgrupo) {
														do { 
															$id_subgrupo = $row_rs_subgrupo['id_subgrupo'];
															$subgrupo_nombre = $row_rs_subgrupo['subgrupo_nombre']; 

															echo '<option value="'.$id_subgrupo.'->'.$subgrupo_nombre.'">'.$subgrupo_nombre.'</option>';
															?>
															<?php } while ($row_rs_subgrupo = mysql_fetch_assoc($rs_subgrupo));
														} ?>	
													</select></p>
													<br>
													<input type="hidden" name="subgrupo_id" id="subgrupo_id" value="0" >
													<input type="text" placeholder="Nombre" name="subgrupo_nombre" id="subgrupo_nombre" required>
													<div class="alinear_centro">
													<button class="boton_azul" id="btn_continuar" >Guardar Subcategoría</button>
													</div>
												</form>
											</fieldset>	
										</fieldset>	
									</section>    	
								</div>			
							</fieldset>	
						</section>    	
					</div>
				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->

			<script type="text/javascript">
				function agregar_imagen() {
					$('#imagen_contenedor').hide();
					$('#contenedor_drag_drop').show();	
				}

				Dropzone.options.myAwesomeDropzone = {
					init: function() {
						this.on("complete", function(file) {
							imagen_cargada();
						});
					},
					accept: function(file, done) {
						if ((file.type != "image/jpeg") && (file.type != "image/png")){
							alert("Error! Solo podés cargar imágenes JPG y PNG");
							this.removeAllFiles();
						} else { done(); }
					}
				};

				function imagen_cargada() {
					$('#contenedor_drag_drop').hide();
					$('#imagen_cargando').show();
					$.ajax({
						url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/php/03-mostrar-imagen-categoria.php?categoria="+<?php echo $categoria;?>,
						success: function (resultado) {
							$('#imagen_cargando').html(resultado);
			//refrescamos para que se borren los temporales
			setInterval( function() {
				window.open('<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/08-ficha-categoria.php?categoria=<?php echo $categoria;?>', '_self');
			}, 1500 );
		}
	});	
				}

				function borrar_imagen() {
					$('#popup_categoria').addClass('is-visible');
				}

				function confirmar_borrado_imagen() {
					$('#imagen_contenedor').hide();
					$('#imagen_cargando').show();
					$('.cd-popup').removeClass('is-visible');

					$.ajax({
						url: "<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-general/php/03-borrar-imagen-categoria.php?categoria="+<?php echo $categoria;?>,
						success: function (resultado) {
							$('#imagen_cargando').hide();
							$('#contenedor_drag_drop').show();
						}
					});			
				}

				function cerrar_popup() {
					$('.cd-popup').removeClass('is-visible');
				}

				function analizar_select() {
					subgrupo = $('#subgrupo').val();

					nombre = subgrupo.split('->');

					$('#subgrupo_id').val(nombre[0]);
					$('#subgrupo_nombre').val(nombre[1]);
				}
			</script>
		</body>
		</html>