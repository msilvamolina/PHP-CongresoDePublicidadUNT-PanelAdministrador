<?php include('../../../paginas_include/variables-generales.php'); 

$id_usuario = trim($_GET['usuario']);
//consultar en la base de datos
conectar2('congreso', "sitioweb");

if($id_usuario) {
	$query_rs_usuario = "SELECT usuario_codigo_barras, usuario_dni, usuario_nombre, usuario_apellido  FROM usuarios WHERE id_usuario = $id_usuario ";
	$rs_usuario = mysql_query($query_rs_usuario)or die(mysql_error());
	$row_rs_usuario = mysql_fetch_assoc($rs_usuario);
	$totalrow_rs_usuario = mysql_num_rows($rs_usuario);
} else {
	$query_rs_usuario = "SELECT id_usuario, usuario_codigo_barras, usuario_dni, usuario_nombre, usuario_apellido  FROM usuarios WHERE usuario_codigo_barras IS NOT NULL AND usuario_nombre IS NOT NULL ORDER BY id_usuario DESC LIMIT 1";
	$rs_usuario = mysql_query($query_rs_usuario)or die(mysql_error());
	$row_rs_usuario = mysql_fetch_assoc($rs_usuario);
	$totalrow_rs_usuario = mysql_num_rows($rs_usuario);

	$id_usuario = $row_rs_usuario['id_usuario'];
}

$url_credenciales = $Servidor_url."PANELADMINISTRADOR/00-barra-navegacion/credenciales/00-imprimir-credenciales.php?usuario=";

$link_anterior = "javascript:no_hay()";
$link_siguiente = "javascript:no_hay()";

//consultar en la base de datos
$query_rs_usuario_anterior = "SELECT id_usuario FROM usuarios WHERE id_usuario < $id_usuario AND usuario_codigo_barras IS NOT NULL AND usuario_nombre IS NOT NULL ORDER BY id_usuario DESC LIMIT 1";
$rs_usuario_anterior = mysql_query($query_rs_usuario_anterior)or die(mysql_error());
$row_rs_usuario_anterior = mysql_fetch_assoc($rs_usuario_anterior);
$totalrow_rs_usuario_anterior = mysql_num_rows($rs_usuario_anterior);

if($totalrow_rs_usuario_anterior) {
	$id_usuario_anterior = $row_rs_usuario_anterior['id_usuario'];
	$link_anterior = $url_credenciales.$id_usuario_anterior;
}

//consultar en la base de datos
$query_rs_usuario_siguiente = "SELECT id_usuario FROM usuarios WHERE id_usuario > $id_usuario AND usuario_codigo_barras IS NOT NULL AND usuario_nombre IS NOT NULL ORDER BY id_usuario ASC LIMIT 1";
$rs_usuario_siguiente = mysql_query($query_rs_usuario_siguiente)or die(mysql_error());
$row_rs_usuario_siguiente = mysql_fetch_assoc($rs_usuario_siguiente);
$totalrow_rs_usuario_siguiente = mysql_num_rows($rs_usuario_siguiente);

if($totalrow_rs_usuario_siguiente) {
	$id_usuario_siguiente = $row_rs_usuario_siguiente['id_usuario'];
	$link_siguiente = $url_credenciales.$id_usuario_siguiente;
}


desconectar();

$usuario_codigo_barras = $row_rs_usuario['usuario_codigo_barras'];
$usuario_dni = $row_rs_usuario['usuario_dni'];
$usuario_nombre = $row_rs_usuario['usuario_nombre']." ".$row_rs_usuario['usuario_apellido'];

//$usuario_nombre = "este es un nombre super largo para probar como anda todo";
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://www.congresodepublicidadunt.com/PANELADMINISTRADOR/css/reset2.css?v=1"> <!-- CSS reset -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	<style type="text/css">
		* {
			font-family: 'Open Sans', sans-serif;

		}
		.credencial_contenedor, .credencial {
			width: 620px;
			height: 874px;
			z-index: 1;
		}
		.codigo_barra {
			z-index: 5;
			position: absolute;
			margin-top: 613px;
			margin-left: 40px;
			width: 540px;
		}
		.nombre {
			font-weight: bold;
			position: absolute;
			margin-top: 400px;
			margin-left: 325px;
			font-size: 34px;
			width: 240px;
		}

		.botones {
			width: 612px;
		}

		.botones a {
			padding: 2.5%;
			width: 45%;
			float: left;
			text-decoration: none;
			font-weight: bold;
		}

		.siguiente {
			text-align: right;
			background: #EEEEEE;
		}
		.clear {
			float: none;
			clear: both;
		}

	</style>
	<link rel="stylesheet" href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/credenciales/print-style.css" type="text/css" media="print" />

</head>

<body>
	<div class="botones">
		<a href="<?php echo $link_anterior; ?>"><- Anterior</a>
		<a href="<?php echo $link_siguiente; ?>" class="siguiente">Siguiente -></a>
		<div class="clear"></div>
	</div>
	<div class="credencial_contenedor">
		<div class="nombre"><?php echo $usuario_nombre; ?><br><br><?php echo $usuario_dni; ?></div>
		<img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/credenciales/barcode.php?text=<?php echo $usuario_codigo_barras; ?>&size=90&orientation=horizontal&print=true&sizefactor=2" class="codigo_barra" />
		<img src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/credenciales/credencial4.png" class="credencial">
	</div>
</body>
<script type="text/javascript">
	function no_hay() {
		alert("No hay más");
	}
</script>
</html>