<?php include('../../../../paginas_include/variables-generales.php'); 
include('../../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../../php/verificar-permisos.php');

$evento = trim($_POST['evento']);
$nombre = trim($_POST['nombre']);
$asistencia = trim($_POST['asistencia']);

$descripcion = trim($_POST['descripcion']);
$conferencista = trim($_POST['evento_conferencista']);
$lugar = trim($_POST['evento_lugar']);
$fecha = trim($_POST['fecha']);
$hora = trim($_POST['hora']);
$hora2 = trim($_POST['hora2']);
$color = trim($_POST['color']);
$color_texto = trim($_POST['evento_color_texto']);

$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/01-editar-evento.php?evento='.$evento;

if(!$nombre || !$evento || !$fecha) {
	header('Location: '.$redireccion);
	exit;
}

$nombre = arreglar_datos_db($nombre);

conectar2('congreso', "aplicacion");

$explorar_hora = explode(':', $hora);

if($explorar_hora[2]) {
	$nueva_hora = $hora;
} else {
	$nueva_hora = $hora.':00';
}

if($asistencia) {
	$asistencia = 1;
} else {
	$asistencia = 0;
}

$evento_fecha = $fecha.' '.$nueva_hora;

$fecha_actual = date('Y-m-d H:i:s');
mysql_query("UPDATE eventos SET evento_nombre='$nombre', tomar_asistencia='$asistencia', id_conferencista='$conferencista', id_sala='$lugar', evento_descripcion='$descripcion', evento_fecha='$evento_fecha', evento_hora_fin='$hora2', evento_color='$color', evento_color_texto='$color_texto',usuario_que_modifica='$id_administrador', ip_visitante_modificacion='$ip_visitante', fecha_modificacion='$fecha_actual' WHERE id_evento='$evento'");

desconectar();

header('Location: '.$redireccion.'&ok=1');
exit;
?>