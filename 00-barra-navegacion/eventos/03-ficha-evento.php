<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');
$evento = trim($_GET['evento']);
$redirigir = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/02-eventos.php';
$imagen_cargada = trim($_GET['imagen_cargada']);

if(!$evento) {
	header('location:'.$redirigir);
	exit;
} 
conectar2('congreso', "aplicacion");

//consultar en la base de datos
$query_rs_eventos = "SELECT *  FROM eventos WHERE id_evento = $evento ";
$rs_eventos = mysql_query($query_rs_eventos)or die(mysql_error());
$row_rs_eventos = mysql_fetch_assoc($rs_eventos);
$totalrow_rs_eventos = mysql_num_rows($rs_eventos);

$id_evento = $row_rs_eventos['id_evento'];
$evento_sala = $row_rs_eventos['id_sala'];
$evento_conferencista = $row_rs_eventos['id_conferencista'];
$evento_nombre = $row_rs_eventos['evento_nombre'];
$evento_descripcion = $row_rs_eventos['evento_descripcion'];
$evento_fecha = $row_rs_eventos['evento_fecha'];
$evento_hora_fin = $row_rs_eventos['evento_hora_fin'];
$evento_color = $row_rs_eventos['evento_color'];
$evento_color_texto = $row_rs_eventos['evento_color_texto'];

if(!$evento_color) {
	$evento_color = "(Vacío)";
}

if($evento_sala) {
	//consultar en la base de datos
	$query_rs_sala = "SELECT * FROM salas WHERE id_sala = $evento_sala ";
	$rs_sala = mysql_query($query_rs_sala)or die(mysql_error());
	$row_rs_sala = mysql_fetch_assoc($rs_sala);
	$totalrow_rs_sala = mysql_num_rows($rs_sala);

	$nombre_sala = $row_rs_sala['sala_nombre'];
}

if($evento_conferencista) {
//consultar en la base de datos
	$query_rs_conferencistas = "SELECT conferencista_nombre FROM conferencistas WHERE id_conferencista = $evento_conferencista ";
	$rs_conferencistas = mysql_query($query_rs_conferencistas)or die(mysql_error());
	$row_rs_conferencistas = mysql_fetch_assoc($rs_conferencistas);
	$totalrow_rs_conferencistas = mysql_num_rows($rs_conferencistas);

	$conferencista_nombre = $row_rs_conferencistas['conferencista_nombre'];
}


	//consultar en la base de datos
$query_rs_imagen = "SELECT id_foto, nombre_foto, fecha_carga, recorte_foto_nombre, recorte_foto_miniatura FROM fotos_publicaciones WHERE id_evento = $evento ORDER BY id_foto DESC ";
$rs_imagen = mysql_query($query_rs_imagen)or die(mysql_error());
$row_rs_imagen = mysql_fetch_assoc($rs_imagen);
$totalrow_rs_imagen = mysql_num_rows($rs_imagen);

if($imagen_cargada) {
	$id_foto = $row_rs_imagen['id_foto'];
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/informacion-general/03-recortar-foto.php?origen=ficha&foto='.$id_foto.'&evento='.$evento;
	header('location:'.$redireccionar);
	exit;
}

if($totalrow_rs_imagen) {
	$id_foto_portada = $row_rs_imagen['id_foto'];
	$foto_portada = $row_rs_imagen['recorte_foto_nombre'];
}

desconectar();

$usuario_que_carga = $row_rs_eventos['usuario_que_carga'];
$usuario_que_modifica = $row_rs_eventos['usuario_que_modifica'];
$fecha_modificacion = $row_rs_eventos['fecha_modificacion'];
$array_datos['Descripción'] = $evento_descripcion;
$array_datos['Conferencista'] = $conferencista_nombre;
$array_datos['Lugar'] = $nombre_sala;

$array_datos['Fecha de carga'] = nombre_fecha_min($row_rs_eventos['fecha_carga']);
if($fecha_modificacion) {
	$array_datos['Fecha modificación'] =  nombre_fecha_min($fecha_modificacion);
}
$link_redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/salas/03-ficha-sala.php?';

$ruta_imagenes = $Servidor_url.'APLICACION/Imagenes/eventos/';

$imagen = $Servidor_url.'PANELADMINISTRADOR/img/icono-imagen.png';
$nombre_imagen = '<span class="no_hay_imagen">(no hay imagen)</span>';
$icono_boton = 'add.png';
$link_boton = 'agregar_imagen(1)';

if($foto_portada) {
	$imagen = $ruta_imagenes.'recortes/'.$foto_portada;
	$icono_boton = 'delete.png';
	$link_boton = 'borrar_imagen('.$id_foto_portada.')';
}

$explorar_hora = explode(":", $evento_hora_fin);
$evento_hora_fin = $explorar_hora[0].':'.$explorar_hora[1];
$evento_fecha = nombre_fecha_min($evento_fecha)." hasta las ".$evento_hora_fin;



$array_color_texto["#000000"] = "Negro";
$array_color_texto["#ffffff"] = "Blanco";

?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/paginacion.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/barra-pasos.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/negocios.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/fichas.css"> <!-- Resource style -->

	<script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyA51ZllTKq7CDMNNYUro72e6TnJ8RtEQa4'></script>

	<style type="text/css">
		.tabla_martin {
			width: 100%;
			max-width: 800px;
			margin: 0 auto;
			margin-bottom: 50px;
		}
		.tabla_celda {
			width: 100%;
			text-align: center;
			padding: 10px;
		}	
		.tabla_martin_fila {
			background: #ccc;
		}
		.alinear_derecha {
			text-align: center;
		}
		.alinear_derecha {
			background: #ff6000;
			color: #fff;
			padding: 10px;
		}
		@media only screen and (min-width: 768px) {
			.tabla_celda {
				text-align: left;
				width: 50%;
				float: left;
			}
			.alinear_derecha {
				text-align: right;
			}
		}

		h2 {
			padding-bottom: 0px;
			font-size: 26px;
			text-align: center;
		}
		.vacio {
			color: #c6c6c6;
		}
		.boton_verde a{
			background: #48b617;
			color: #fff;
		}
		.boton_verde a:hover {
			background: #235d09 !important;
			color: #f6ff05;
		}	
		.boton_rojo a{
			background: #c40000;
			color: #fff;
		}
		.boton_rojo a:hover {
			background: #9e0101 !important;
			color: #f6ff05;
		}		
		h2 span {
			background: red;
			padding: 5px;
			border-radius: 5px;
			color: #fff;
		}
		.h2_direcciones {
			padding: 20px;
			padding-bottom: 0px;
		}
		h2 .h2_direcciones {
			margin-top: 40px;
			display: block;
		}
		.clear {
			clear: both;
		}
		.sin_fondo {
			background: #fff !important;
			color: #ff6000!important;
		}
		.fondo_celeste {
			background: #3a86f6 !important;
		}

		.area_texto{
			width: 100%;
		}
		#otro_problema {
			display: none;
		}
		#reportar_problema {
			width: 100%;
			max-width: 300px;
			margin: 0 auto;
		}
		#reportar_problema li{
			padding: 10px;
			background: #790054;
			color: #fff;
			cursor: pointer;
		}
		#reportar_problema li:nth-of-type(2n){
			background: #c400a3;
		}	
		.area_texto {
			color: #000;
			height: 200px;
		}

		.mostrar_otro_poblema {
			display: none;
		}
		#reportar_problema {
			display: none;
		}
		.ul_encabezado {
			color:#ffea00 !important;
			font-size: 20px;
			text-align: center;
		}

		.negocio_reportado {
			width: 100%;
			max-width: 600px;
			margin: 20px auto;
		}	
		.mapa{
			width: 100%;
			height: 300px;
		}	

		.usuario_avatar {
			width: 50px;
			border-radius: 50%;
		}
		td {
			cursor: pointer;
		}

		.fa-toggle-on {
			font-size: 30px;
			color: #03af4f;
			cursor: pointer;
		}
		.fa-toggle-off {
			font-size: 30px;
			color: #f98c96;
			cursor: pointer;
		}	

		a {
			cursor: pointer;
		}

		.select_class {
			width: 100%;
			padding: 10px;
		}

		.imagen_portada {
			background: #8de4e1;
			height: auto;
			width: 100%;
		}

		.fondo_verde {
			color: #FFF9C4;
			background: #8BC34A;
		}

		.fondo_verde2 {
			background: #CDDC39;
		}
		.fondo_purpura {
			background: #7E57C2;
		}
		.fondo_purpura2 {
			background: #B39DDB;
			color: #FFF9C4;
		}

		.fondo_naranja2 {
			background: #FFB74D;
		}

		.tabla_celda {
			font-weight: normal;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<div class="cd-popup" id="popup_categoria" role="alert">
			<div class="cd-popup-container">
				<p>¿Estás seguro de querer borrar esta imagen?</p>
				<ul class="cd-buttons">
					<li id="btn_confirmar_categoria"><a onclick="">Sí</a></li>
					<li><a onclick="cerrar_popup()">No</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->		

		<div class="cd-popup" id="popup_borrar" role="alert">
			<div class="cd-popup-container">
				<p>¿Estás seguro de querer borrar este negocio?</p>
				<ul class="cd-buttons">
					<li id="btn_confirmar_borrado"><a onclick="confirmar_borrado()">Sí</a></li>
					<li><a onclick="cerrar_popup()">No</a></li>
				</ul>
				<a href="#0" class="cd-popup-close img-replace"></a>
			</div> <!-- cd-popup-container -->
		</div> <!-- cd-popup -->
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper" >
			<!-- Contenido de la Pagina-->
			<nav role="navigation">
				<ul class="cd-pagination">
					<li class="button"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo ($evento-1); ?>">Anterior</a></li>
					<li class="button"><a  href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo ($evento+1); ?>">Siguiente</a></li>
				</ul>
			</nav> <!-- cd-pagination-wrapper -->		

			<h2 style="background-color: <?php echo $evento_color; ?>; color: <?php echo $evento_color_texto; ?>"><?php echo $id_evento.'<br><br>'.$evento_nombre.'<br><br><b>'.$evento_fecha; ?></h2></h2>

			<nav role="navigation">
				<ul class="cd-pagination">
					<li class="button boton_verde"><a href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/01-editar-evento.php?evento=<?php echo $evento; ?>">Editar</a></li>		
					<li class="button boton_rojo"><a href="#" onclick="borrar_negocio()">Borrar</a></li>	
				</ul>
			</nav> <!-- cd-pagination-wrapper -->

			<div class="tabla_martin">
				<div class="imagen_portada">
					<div class="imagen_delete">
						<a href="#" onclick="<?php echo $link_boton; ?>">
							<img src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/img/<?php echo $icono_boton;?>" class="icono_boton">
						</a>
					</div>
					<center>
						<img src="<?php echo $imagen; ?>" class="imagen_portada2" />
					</center>
				</div>
				<br><br>
				<div clas="tabla_martin_fila">
					<div class="tabla_celda alinear_derecha ">
						Nombre del evento
					</div>
					<div class="tabla_celda fondo_naranja2"><?php echo $evento_nombre; ?></div>
					<div class="tabla_celda alinear_derecha fondo_purpura">
						Fecha del evento
					</div>
					<div class="tabla_celda fondo_purpura2"><?php echo $evento_fecha; ?></div>
					<div class="tabla_celda alinear_derecha sin_fondo">
						Color
					</div>
					<div class="tabla_celda" style="background-color: <?php echo $evento_color; ?>"><b style="color: <?php echo $evento_color_texto; ?>"><?php echo $evento_color; ?></b></div>
				</div>

				<?php foreach ($array_datos as $nombre => $dato) { ?>
				<div clas="tabla_martin_fila">
					<div class="tabla_celda alinear_derecha sin_fondo"><?php echo $nombre; ?></div>

					<div class="tabla_celda"><?php
						if(is_array($dato)) {
							foreach ($dato as $clave => $valor) {
								if($valor) {
									$explorar_clave = explode('https://', $clave);
									if($explorar_clave[1]) {
										$link = $clave;
									} else {
										$link = $link_redireccion.$clave;
									}
									echo '<a href="'.$link.'">'.$valor.'</a>';
								} else {
									echo '<i class="vacio">(Vacío)</i>';
								}
							}
						} else {
							if($dato) {
								echo $dato;
							} else {
								echo '<i class="vacio">(Vacío)</i>';
							} 
						}	?></div>
						<br><br><br><br>

					</div>
					<?php } ?>

				</div> <!-- .content-wrapper -->
			</main> 
			<?php include('../../includes/pie-general.php');?>
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
			<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
			<script type="text/javascript">
				function borrar_negocio() {
					$('#popup_borrar').addClass('is-visible');
				}
				function confirmar_borrado() {
					window.open('<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/php/02-borrar-evento-db.php?evento=<?php echo $evento;?>', '_self');
				}
				function cerrar_popup() {
					$('.cd-popup').removeClass('is-visible');
				}

				function agregar_imagen(portada) {
					window.open('<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/informacion-general/popUps/02-cargar-imagen-evento.php?evento=<?php echo $evento; ?>','popup','width=800,height=400');
				}
				function borrar_imagen(imagen) {
					$('#popup_categoria').addClass('is-visible');
					$('#btn_confirmar_categoria').html('<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventos/php/03-borrar-imagen-evento.php?evento=<?php echo $evento;?>&foto='+imagen+'">Sí</a>');
				}
			</script>
		</body>
		</html>