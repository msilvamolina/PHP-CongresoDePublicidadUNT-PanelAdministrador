<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$evento = trim($_GET['evento']);

if(!$evento) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/00-cargar-evento.php';
	header('Location: '.$redireccion);
	exit;
}

conectar2('congreso', "aplicacion");

//consultar en la base de datos
$query_rs_evento = "SELECT id_sala, tomar_asistencia, id_conferencista, evento_color, evento_color_texto, evento_hora_fin, evento_nombre, evento_fecha, evento_descripcion  FROM eventos WHERE id_evento = $evento ";
$rs_evento = mysql_query($query_rs_evento)or die(mysql_error());
$row_rs_evento = mysql_fetch_assoc($rs_evento);
$totalrow_rs_evento = mysql_num_rows($rs_evento);

$evento_lugar = $row_rs_evento['id_sala'];
$evento_conferencista = $row_rs_evento['id_conferencista'];

$asistencia = $row_rs_evento['tomar_asistencia'];

$evento_tipo = $row_rs_evento['evento_tipo'];
$evento_nombre = $row_rs_evento['evento_nombre'];
$evento_fecha = $row_rs_evento['evento_fecha'];
$evento_hora_fin = $row_rs_evento['evento_hora_fin'];
$evento_color = $row_rs_evento['evento_color'];
$evento_color_texto = $row_rs_evento['evento_color_texto'];

$evento_descripcion = $row_rs_evento['evento_descripcion'];

//consultar en la base de datos
$query_rs_salas = "SELECT id_sala, sala_nombre FROM salas ORDER BY sala_nombre ASC ";
$rs_salas = mysql_query($query_rs_salas)or die(mysql_error());
$row_rs_salas = mysql_fetch_assoc($rs_salas);
$totalrow_rs_salas = mysql_num_rows($rs_salas);

//consultar en la base de datos
$query_rs_conferencistas = "SELECT id_conferencista, conferencista_nombre FROM conferencistas ORDER BY conferencista_nombre ASC ";
$rs_conferencistas = mysql_query($query_rs_conferencistas)or die(mysql_error());
$row_rs_conferencistas = mysql_fetch_assoc($rs_conferencistas);
$totalrow_rs_conferencistas = mysql_num_rows($rs_conferencistas);

desconectar();

if(!$totalrow_rs_evento) {
	$redireccion = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/eventos/00-cargar-evento.php';
	header('Location: '.$redireccion);
	exit;
}


$explorar_fecha = explode(' ', $evento_fecha);
$fecha_actual = $explorar_fecha[0];
$hora_actual = $explorar_fecha[1];

if(!$evento_hora_fin) {
	$evento_hora_fin = $hora_actual;
}


$array_color_texto["#000000"] = "Negro";
$array_color_texto["#ffffff"] = "Blanco";
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form3.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	<script src="<?php echo $Servidor_url;?>PANELADMINISTRADOR/js/nicEdit2/nicEdit.js" type="text/javascript"></script>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/checkbox/style.css?v=3"> <!-- Resource style -->
	<link rel="stylesheet" href="http://www.congresodepublicidadunt.com/PANELADMINISTRADOR/css/botones.css"> <!-- Resource style -->

	<style type="text/css">
		.contenedor{
			text-align: center;
			margin: 0 auto;
			padding-top: 40px;
		}
		.bender{
			width: 100%;
			max-width: 134px;
		}

		h2 {
			margin-top: 10px;
			font-size: 26px;
		}
		.td_delete {
			padding: 10px;
			text-align: right;
			width: 30px;
		}
		.td_delete img {
			width: 30px;
			display: block;
		}	
		.cd-form {
			text-align: left;
		}

		.grupo_categoria {
			margin-left: -20px!important;
		}

		#section_categoria {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;
		}

		#section_categoria h3 {
			font-size: 24px;
		}
		.select_class {
			background: #eeeeee !important;
		}

		.section_informacion_bender {
			background: #a7a7a7;
			padding: 30px;
			color: #fff;		
		}

		#section_informacion_bender {
			line-height: 18px;
		}
		.bender_chiquito {
			width: 30px;
		}

		.info_bender {
			margin-bottom: 20px;
		}
		.delete_categoria {
			float: right;
		}
		.delete_categoria img{
			width: 35px;
			margin-top: -5px;
		}
		#negocio_repetido {
			width: 100%;
			padding: 30px;
			background: #464646;
			color:#fff;
		}
		#negocio_repetido b {
			color: #e6d461;
		}

		#negocio_repetido span {
			color: #f92672;
		}

		#negocio_repetido h3 {
			color: #a6db29;
			font-size: 32px;
			margin-bottom: 10px;
		}

		.campo_fecha_hora {
			padding: 6px;
			border: 1px solid #B3E5FC;
			background-color: #ffffff;
			border-radius: .25em;
			box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.08);
			width: 49.6%;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">

			<div class="contenedor">

				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels">
						<section id="crear_categoria" >							
							<fieldset >
								<?php if($_GET['ok']) { ?>
								<div class="alert alert-success" role="alert">
									Los cambios se guardaron correctamente a las <?php echo date('H:i:s'); ?>
								</div>
								<?php } ?>
								<form id="myForm" onsubmit="return validar_formulario()" action="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/eventos/php/01-editar-evento-db.php" method="POST">
									<input  type="hidden" id="evento" name="evento" value='<?php echo $evento; ?>'>

									<a href="<?php echo $Servidor_url;?>PANELADMINISTRADOR/00-barra-navegacion/eventos/03-ficha-evento.php?evento=<?php echo $evento;?>" class="vc_btn_largo vc_btn_rojo vc_btn_3d" style="width:250px;float:right">
										<span class="fa-stack fa-lg pull-left">
											<i class="fa fa-circle fa-stack-2x"></i>
											<i class="fa fa-calendar-o fa-stack-1x fa-inverse"></i>
										</span>
										<b>Ficha del evento</b>
									</a>
									<legend id="txt_nueva_categoria">Editar Evento</legend>

									<?php 
									$checkbox = "";
									if($asistencia) {
										$checkbox = "checked";
									}
									?>
									<input type="checkbox" <?php echo $checkbox; ?> name="asistencia" id="asistencia" class="css-checkbox" /><label for="asistencia" class="css-label">Tomar Asistencia</label>
									<br><br><br>
									<div class="icon">
										<label class="cd-label" for="cd-company">Nombre del evento</label>
										<input class="company" type="text" name="nombre" value="<?php echo $evento_nombre; ?>" id="nueva_categoria_nombre" required>
									</div> 			    
									
									<p class="cd-select icon"><label class="cd-label" for="cd-company">Conferencista</label>
										<select name="evento_conferencista"  class="budget"   >				<option value="">Elegí un conferencista</option>	
											<?php
											do {
												$id_conferencista = $row_rs_conferencistas['id_conferencista'];
												$conferencista_nombre = $row_rs_conferencistas['conferencista_nombre'];

												$elegido = null;
												if($evento_conferencista==$id_conferencista) {
													$elegido = 'selected';
												}
												echo '<option value="'.$id_conferencista.'" '.$elegido.'>'.$conferencista_nombre.'</option>';
											} while ($row_rs_conferencistas = mysql_fetch_assoc($rs_conferencistas));
											?>
										</select></p>

										<p class="cd-select icon"><label class="cd-label" >Sala</label>
											<select name="evento_lugar"  class="budget">
												<option value="">Elegí una sala</option>	
												<?php
												do {
													$id_sala = $row_rs_salas['id_sala'];
													$sala_nombre = $row_rs_salas['sala_nombre'];

													$elegido = null;
													if($evento_lugar==$id_sala) {
														$elegido = 'selected';
													}
													echo '<option value="'.$id_sala.'" '.$elegido.'>'.$sala_nombre.'</option>';
												} while ($row_rs_salas = mysql_fetch_assoc($rs_salas));
												?>
											</select></p>
											<div class="icon" id="fecha_noticia" >
												<label class="cd-label" for="cd-company">Fecha evento</label>
												<input class="company" type="date" value="<?php echo $fecha_actual; ?>" step="1" name="fecha"   required>
												<label class="cd-label" for="cd-company">Hora de Comienzo y Finalización</label>
												<input class="campo_fecha_hora company" type="time" value="<?php echo $hora_actual; ?>" step="1" name="hora"  required>
												<input class="campo_fecha_hora company" type="time" value="<?php echo $evento_hora_fin; ?>" step="1" name="hora2"  required>
											</div>
											<br>
											<div class="icon">
												<label class="cd-label" for="cd-company">Color de fondo</label>
												<input class="company" type="text" name="color" value="<?php echo $evento_color; ?>" required>
											</div> 

											<p class="cd-select icon"><label class="cd-label" for="cd-company">Color del texto</label>
												<select name="evento_color_texto"  class="budget"   >			
													<?php
													foreach ($array_color_texto as $clave => $tipo) {
														$elegido = null;
														if($evento_color_texto==$clave) {
															$elegido = 'selected';
														}
														if($tipo) {
															echo '<option value="'.$clave.'" '.$elegido.'>'.$tipo.'</option>';
														}
													}
													?>
												</select></p>

												<div id="cuerpo" style="background: #fff">
													<h3>Descripción:</h3>
													<table class="table table-striped">
														<tbody>
															<tr>

																<td>
																	<textarea name="descripcion" id="area<?php echo $i;?>"><?php echo $evento_descripcion; ?></textarea>

																</td>

																<script type="text/javascript">
																	bkLib.onDomLoaded(function() {
																		new nicEditor({fullPanel : true}).panelInstance('area<?php echo $i;?>');
																	});
																</script>
															</tr>	

														</tbody>
													</table>
												</div>
												<br><br><br>

												<div class="alinear_centro">
													<input type="submit" value="Guardar Cambios">
												</div>
											</div>


										</form>
									</fieldset>
								</section>

								<br><br><br><br><br>
							</div>

						</div> <!-- .content-wrapper -->


					</main> 
					<?php include('../../includes/pie-general.php');?>
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
					<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->


					<script type="text/javascript">

						function cerrar_popup() {
							$('.cd-popup').removeClass('is-visible');
						}

						function validar_formulario() {
							var error = null;	

							if(error) {
								alert(error);
								return false;
							} else {
								return true;
							}
						}
					</script>
				</body>
				</html>