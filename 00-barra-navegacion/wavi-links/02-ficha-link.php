<?php include('../../../paginas_include/variables-generales.php'); 
include('../../includes/permisos-usuarios.php');
$permisos_pagina = '';
include('../../php/verificar-permisos.php');

$link = trim($_GET['link']);

if(!$link) {
	$redireccionar = $Servidor_url.'PANELADMINISTRADOR/00-barra-navegacion/wavi-links/01-links-diarios.php';
	header('location:'.$redireccionar);
	exit;
}
conectar2('mywavi', 'sitioweb');

//consultar en la base de datos
$query_rs_link = "SELECT id_diario, link_tipo, link_dato  FROM links_rss WHERE id_link = $link ";
$rs_link = mysql_query($query_rs_link)or die(mysql_error());
$row_rs_link = mysql_fetch_assoc($rs_link);
$totalrow_rs_link = mysql_num_rows($rs_link);

$id_diario = $row_rs_link['id_diario'];
$link_tipo = $row_rs_link['link_tipo'];
$link_dato = $row_rs_link['link_dato'];

desconectar();

include('php/xml-to-array.php');
require_once('OpenGraph.php');

if($link_tipo=='twitter') {
	include('php/analizar-twitter.php');
} 

if($link_tipo=='rss') {
	include('php/analizar-rss.php');
}
?>
<!doctype html>
<html lang="en" class="no-js">
<head>
	<?php include('../../includes/head-general.php'); ?>
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/form.css"> <!-- Resource style -->
	<link rel="stylesheet" href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/css/popup.css"> <!-- Resource style -->
	
	<style type="text/css">
		.tabla_encabezado {
			color: red;
		}

		tr {
			cursor: pointer;
		}

		.categorias_con_subgrupos {
			background: #f90 !important;
			color: #fff !important;
		}
	</style>
</head>
<body>
	<?php include('../../includes/header.php'); ?>
	<main class="cd-main-content">
		<?php include('../../includes/barra-navegacion.php'); ?>
		<div class="content-wrapper">
			<div class="contenedor">

				<div >					<!-- Contenido de la Pagina-->	
					<div class="cd-form floating-labels" style="max-width:1600px">
						<table class="table table-striped">
							<thead class="tabla_encabezado">
								<tr>
									<th><b>#</b></th>
									<th><b>Título</b></th>
									<th><b>Descripción</b></th>
									<th><b>Link</b></th>
									<th><b>Fecha</b></th>
								</tr>
							</thead>
							<tbody>
								<?php if($nuevo_array) {
									$i=1; foreach ($nuevo_array as $clave => $valor) {
										$link = trim($valor['link']);

										$array_reemplazar = array("http://www.perfil.com/http://");
										$array_reemplazar2 = array("http://");

										$link = str_replace($array_reemplazar, $array_reemplazar2, $link);

										?>
										<tr class="<?php echo $super_class; ?>" data-href="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/00-barra-navegacion/wavi-links/03-analizar-link.php?link=<?php echo $link; ?>">
											<td><?php echo $i; ?></td>
											<td><?php echo $valor['title']; ?></td>
											<td><?php echo $valor['description']; ?></td>
											<td><?php echo $link; ?></td>
											<td><?php echo $valor['date']; ?></td>
										</tr>		
										<?php $i++;}} ?>
									</tbody>
								</table>				 

							</div>
						</div>
					</div> <!-- .content-wrapper -->
				</main> 
				<?php include('../../includes/pie-general.php');?>
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/form.js"></script> <!-- Resource jQuery -->
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/popup.js"></script> <!-- Resource jQuery -->
				<script src="<?php echo $Servidor_url; ?>PANELADMINISTRADOR/js/jquery.ddslick.min.js"></script> <!-- Resource jQuery -->
				<script type="text/javascript">

					$('tr[data-href]').on("click", function() {
						document.location = $(this).data('href');
					});
				</script>
			</body>
			</html>